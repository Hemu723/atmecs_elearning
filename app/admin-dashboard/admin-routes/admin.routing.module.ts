import { NgModule } from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';

import { allCategorycomponent } from '../categories/allCategories/allCategories.component';
import { CoursesComponent } from '../courses/allCourses/courses.component';
import { VideoListComponent } from '../videos/videoList/videoList.component';
import { AllQuizComponent } from '../quiz/allquiz/allquiz.component';
import { getAllUserQuizComponent } from '../getAllUserQuiz/getAllUserQuiz.component';
import { CreateCategoryComponent } from '../categories/createcategory/createCategory.component';
import { CreateCoursesComponent } from '../courses/createcourses/createCourses.component';
import { CreateVideoComponent } from '../videos/createVideo/createVideo.component';
import { CreateQuizQuestionComponent } from '../quiz/addQuiz/addQuestion.component';
import { UpdateCategoryComponent } from '../categories/updateCategory/updateCategory.component';
import { UpdateCoursesComponent } from '../courses/updateCourses/updateCourses.component';
import { UpdateVideoComponent } from '../videos/updateVideo/updateVideo.component';
import { EditQuizQuestionComponent } from '../quiz/updateQuiz/editquiz.component';

import { allCategoryService } from '../categories/allCategories/allCategories.service';
import { CoursesService } from '../courses/allCourses/courses.service';
import { VideoService } from '../videos/videoList/videoList.service';
import { quizService } from '../quiz/allquiz/allquiz.service';
import { UsersQuizService } from '../getAllUserQuiz/getAllUserQuiz.service';
import { CreateCategoryService } from '../categories/createcategory/createCategory.service';
import { CreateCourseService } from '../courses/createcourses/createCourses.service';
import { addQuestionService } from '../quiz/addQuiz/addquestion.service';
import { CategoryService } from '../categories/updateCategory/updateCategory.service';
import { UpdateCourseService } from '../courses/updateCourses/updateCourses.service';
import { UpdateVideoService } from '../videos/updateVideo/updateVideo.service';
import { EditQuestionService } from '../quiz/updateQuiz/editquestion.service';

import { AdminAuthGuard } from '../adminAuthGuard/adminAuthGuard.service';


const adminRoutes: Routes = [
    {
        path: 'admin/category',
        component: UpdateCategoryComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: 'adminDashboardcategory',
        component: allCategorycomponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: 'category/:id',
        component: UpdateCategoryComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: 'createCategory',
        component: CreateCategoryComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: 'createcourses/:categoryId/:categoryname',
        component: CreateCoursesComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: 'courseList/:categoryId/:categoryname',
        component: CoursesComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: 'quizQuestionsList/:courseId/:videoId',
        component: AllQuizComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: 'addQuestion/:courseId/:videoId',
        component: CreateQuizQuestionComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: 'videoList/:categoryId/:courseId',
        component: VideoListComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: 'createVideo/:categoryId/:courseId',
        component: CreateVideoComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: 'updateVideo/:categoryId/:courseId/:videoId',
        component: UpdateVideoComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: 'updateCourses/:courseId/:categoryId/:categoryname',
        component: UpdateCoursesComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: 'updateQuestion/:courseId/:videoId/:_id',
        component: EditQuizQuestionComponent,
        canActivate: [AdminAuthGuard]
      },
      {
        path: 'getAllUserQuiz',
        component: getAllUserQuizComponent,
        canActivate: [AdminAuthGuard]
      }
];

@NgModule({
  imports:  [ RouterModule.forRoot(adminRoutes) ],
  providers: [ 
    AdminAuthGuard, allCategoryService,  CoursesService, VideoService, quizService, UsersQuizService, CreateCategoryService,
    CreateCourseService, addQuestionService, CategoryService, UpdateCourseService, UpdateVideoService, EditQuestionService 
  ],
  exports:  [ RouterModule ]
})

export class AdminRoutingModule { }

export const adminComponents = [
  allCategorycomponent, CoursesComponent, VideoListComponent, AllQuizComponent, getAllUserQuizComponent , CreateCategoryComponent, 
  CreateCoursesComponent, CreateVideoComponent , CreateQuizQuestionComponent, UpdateCategoryComponent, UpdateCoursesComponent, 
  UpdateVideoComponent, EditQuizQuestionComponent
];

