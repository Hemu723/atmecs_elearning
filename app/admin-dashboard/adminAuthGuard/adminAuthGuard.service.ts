import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()

export class AdminAuthGuard implements CanActivate {

    role: string;
    admin: string;

    constructor(private router: Router) { }

    canActivate() {

        this.role = JSON.parse(localStorage.getItem('userProfile')).role;
        this.admin = localStorage.getItem('admin');
        if (this.role == "ADMIN" && this.admin=="administrator") {
            return true;
        }

        else {
            console.log("sorry you dont have admin credentials");
            this.router.navigate(['/pageNotFound']);
            return false;
        }
    }

}