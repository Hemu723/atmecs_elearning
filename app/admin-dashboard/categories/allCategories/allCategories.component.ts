import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

import { allCategoryService } from './allCategories.service';
import { DashboardService } from '../../../user-dashboard/dashboard/dashboard.service';
import { Course } from '../../../shared/models/course.model';

@Component({
    selector: 'adminDashboardCategory',
    templateUrl: './allCategories.view.html'
})

export class allCategorycomponent implements OnInit {
    categories: Course[] = [];
    show: Boolean = false;
    categoryId: string;

    constructor(
        private _allcategoryservice: allCategoryService,
        private _deleteCategoryService: allCategoryService,
        private _router: Router, private sanitizer: DomSanitizer) {
    }

    ngOnInit() {
        this.getAllCategories(JSON.parse(localStorage.getItem('userProfile'))._id);
    }

    getAllCategories(userId: string) {
        this._allcategoryservice.getAllCategories(userId)
            .then(categories => {
                for (let i = 0; i < categories.length; i++) {
                    this.sanitizer.bypassSecurityTrustUrl(categories[i].imgPath.toString());
                }
                this.categories = categories;
            });
    }

    deleteCategory(categoryId: string) {
        this._deleteCategoryService.serviceFunction(categoryId);
        for (let i = 0; i < this.categories.length; i++) {
            if (this.categories[i]._id === categoryId) {
                this.categories.splice(i, 1);
                break;
            }
        }
    }
}