import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { HttpInterceptor } from '../../../shared/httpInterceptor/httpInterceptor';

import { Course } from '../../../shared/models/course.model.js';
import { CourseList } from '../../../shared/models/courseList.model.js';
import 'rxjs/add/operator/toPromise';

@Injectable()

export class allCategoryService {

  constructor(private _http: HttpInterceptor) { }

  getAllCategories(_userId_: string): Promise<Course[]> {
    return this._http.get(`/api/getAllCategories/${_userId_}`)
      .toPromise()
      .then(response => response.json() as Course[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  serviceFunction(_id: string) {
    return this._http.delete(`/api/deleteCategory/${_id}`)
      .toPromise();
  }
}