import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { FormsModule } from '@angular/forms';
import { CreateCategoryService } from './createCategory.service';

@Component({
    selector: 'app-category',
    templateUrl: './createCategory.view.html'
})

export class CreateCategoryComponent {
    categoryId: number;
    categoryName: string;
    categoryDescription: string;
    imageName: string;
    quotes: string;
    uploader: FileUploader;
    submitted = false;
    active = true;


    onSubmit(form: any) {
        this.submitted = true;
    }

    constructor(private _categoryService: CreateCategoryService, private _router: Router) {
        this.uploader = new FileUploader({ url: `/api/uploadImageFile` })
    }

    addCategory() {
        this.uploader.options.url = `/api/uploadImageFile`;
        this.uploader.queue[0].url = `/api/uploadImageFile`;
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            var responsePath = JSON.parse(response);
            this.imageName = responsePath.imagePath;
            let newCategory = {
                "name": this.categoryName,
                "description": this.categoryDescription,
                "imgPath": this.imageName,
                "quotes": this.quotes
            }
            this._categoryService.addCategory(newCategory)
                .then(res => {
                    this._router.navigate(['/adminDashboardcategory'])
                });
        };
        this.uploader.uploadAll();
    }

}
