import { Injectable } from '@angular/core';
import { HttpInterceptor } from '../../../shared/httpInterceptor/httpInterceptor';

import 'rxjs/add/operator/toPromise';

@Injectable()

export class CreateCategoryService {

  constructor(private _http: HttpInterceptor) { }

  addCategory(newCategory: any) {
    return this._http.post('/api/addCategory', newCategory)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError)
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}

