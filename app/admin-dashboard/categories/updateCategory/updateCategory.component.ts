import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';

import { CategoryService } from './updateCategory.service';
import { Course } from '../../../shared/models/course.model';

@Component({
  selector: 'app-category',
  templateUrl: './updateCategory.view.html'
})

export class UpdateCategoryComponent implements OnInit {

  show: Boolean = false;
  _id: string;
  cat: Course;
  uploader: FileUploader;

  constructor(private categoryservice: CategoryService, private _router: Router, private _route: ActivatedRoute) {
    this.cat = new Course('', '', '', '', '', null, true, '');
    this.uploader = new FileUploader({ url: `/api/uploadImageFile` });
  }

  ngOnInit() {
    this._id = this._route.snapshot.params['id'];
    this.categoryservice.serviceFunction(this._id)
      .then(response => {
        this.cat = response;
      });
  }

  editCategory(responseObject: any) {
    this.categoryservice.updateService(responseObject).then
      (res => {
        this._router.navigate(['/adminDashboardcategory']);
      });
  }

  updateCategory() {
    let responseObject =
      {
        "_id": this._id,
        "name": this.cat.name,
        "description": this.cat.description,
        "imgPath":this.cat.imgPath,
        "quotes": this.cat.quotes,
        "status": this.cat.status
      }

    if (this.show) {
      this.uploader.options.url = `/api/uploadImageFile`;
      this.uploader.queue[0].url = `/api/uploadImageFile`;
      this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        var responsePath = JSON.parse(response);
        responseObject.imgPath = responsePath.imagePath;
        this.editCategory(responseObject);
      }
      this.uploader.uploadAll();
    }
    else {
      this.editCategory(responseObject);
    }
  }

  deleteImage(imagePath: string) {
    this.show = true;
    this.categoryservice.deleteImageByImagePath(this.cat.imgPath).then(res => {
    });
  }

}
