import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';

import { HttpInterceptor } from '../../../shared/httpInterceptor/httpInterceptor';
import { Course } from '../../../shared/models/course.model';

@Injectable()

export class CategoryService {

  constructor(private _http: HttpInterceptor) { }

  serviceFunction(_id: any) {
    return this._http.get('/api/getCategoryId/' + _id)
      .toPromise().then(response => response.json().result as Course)
      .catch(this.handleError);
  }

  deleteImageByImagePath(imagePath: String) {
    return this._http.put(`/api/deleteImage`, { "imageURL": imagePath })
      .toPromise().then(response => response.json())
      .catch(this.handleError);
  }

  updateService(requestObject: any) {
    return this._http.put(`/api/updateCategory`, requestObject)
      .toPromise().then(response => response.json())
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}