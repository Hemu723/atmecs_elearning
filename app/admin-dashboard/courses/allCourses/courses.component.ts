import { Component, ViewChild } from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { allCategoryService } from '../../categories/allCategories/allCategories.service';
import { CoursesService } from './courses.service';
import { CourseList } from '../../../shared/models/courseList.model';

@Component({
    selector: 'app-courses',
    templateUrl: './courses.view.html'
})

export class CoursesComponent {
    categoryId: string;
    categoryname: string;
    courses: CourseList[] = [];
    videoLink: string;
    videoName: string;
    videoDesc: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private allCoursesOfCategory: CoursesService
    ) { }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.categoryId = params['categoryId'];
            this.categoryname = params['categoryname'];
            localStorage.setItem('category', this.categoryname);
            this.allCoursesOfCategory.getCourseByCategoryId(this.categoryId)
                .then(res => this.courses = res.result);
        });
    }

    selectedCourse(_videoList: Array<any>, _courseId: string, _courseName: string) {
        localStorage.setItem('selectedCourses', _courseName);
        this.router.navigate(['/videoList', this.categoryId, _courseId]);
    }

    modalOpen(index: number) {
        for (var j = 0; j < this.courses[index].video.length; j++) {
            this.videoLink = this.courses[index].video[j].path;
            this.videoName = this.courses[index].video[j].name;
            this.videoDesc = this.courses[index].video[j].desc;
        }
        this.modal.open('lg');
    }

    @ViewChild('modal')
    modal: ModalComponent;
    ngAfterViewInit() {
    }

    close(event: any) {
        this.videoLink = "";
        this.modal.close();
    }

    open() {
        this.modal.open();
    }

    dismiss() {
        this.modal.dismiss();
    }

    deleteCourse(id: string) {
        console.log(id);
        this.allCoursesOfCategory.deletingCourseService(id);
        for (let i = 0; i < this.courses.length; i++) {
            console.log(id);
            if (this.courses[i]._id === id) {
                this.courses.splice(i, 1);
                break;
            }
        }

    }
}