import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';

import { HttpInterceptor } from '../../../shared/httpInterceptor/httpInterceptor';
import { Course } from '../../../shared/models/course.model';
import { CourseList } from '../../../shared/models/courseList.model';

@Injectable()

export class CoursesService {

    constructor(private _http: HttpInterceptor) { }

    getCourseByCategoryId(course_id: string) {
        return this._http.get(`/api/category/${course_id}`)
            .toPromise().then(response => response.json() as CourseList[])
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    deletingCourseService(_id: string) {
        return this._http.delete(`/api/deleteCourse/${_id}`)
            .toPromise();
    }
}