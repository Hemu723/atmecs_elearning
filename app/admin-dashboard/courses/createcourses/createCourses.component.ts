import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Response } from '@angular/http';
import { FileUploader } from 'ng2-file-upload';
import { FormsModule } from '@angular/forms'
import { FormBuilder, Validators } from '@angular/forms';
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";

import { CreateCourseService } from "./createCourses.service";
import { CourseList } from "../../../shared/models/courseList.model"
import { VideoDescription } from "../../../shared/models/videoDesc.model";

@Component({
    selector: 'app-courses',
    templateUrl: './createCourses.view.html',

})

export class CreateCoursesComponent implements OnInit {
    courseName: string;
    courseDescription: string;
    categoryId: string;
    video: Array<VideoDescription> = [];
    _id: string;
    videoPath: string[] = [];
    courseName1: string;
    categoryName: string;

    constructor(private _courseService: CreateCourseService, private _router: Router, private route: ActivatedRoute, ) {
    }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.categoryId = params['categoryId'];
            this.categoryName = params['categoryname']
        });
    }

    addCourse() {
        let newCourse = {
            "name": this.courseName,
            "description": this.courseDescription,
            "categoryId": this.categoryId
        }
        this._courseService.addCourse(newCourse);
        setTimeout(() => {
            this._router.navigate(['/courseList'+'/'+this.categoryId+'/'+this.categoryName]);
        }, 150);
    }

}
