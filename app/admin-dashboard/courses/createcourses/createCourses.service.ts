import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';

import { HttpInterceptor } from '../../../shared/httpInterceptor/httpInterceptor';
import { VideoDescription } from "../../../shared/models/videoDesc.model";

@Injectable()

export class CreateCourseService {

    constructor(private _http: HttpInterceptor) { }

    addCourse(newCourse:any) {
        return this._http.post('/api/addCourse',newCourse)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError)
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
    
