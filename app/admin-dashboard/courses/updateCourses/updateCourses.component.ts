import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { UpdateCourseService } from './updateCourses.service';
import { CourseList } from '../../../shared/models/courseList.model';
import { VideoDescription } from "../../../shared/models/videoDesc.model";

@Component({
  selector: 'app-courses',
  templateUrl: './updateCourses.view.html'
})

export class UpdateCoursesComponent implements OnInit {
  categoryId: string;
  categoryname: string;
  course: CourseList;
  name: string;
  description: string;
  _id: string;
  status: string;
  video = new VideoDescription('', '', '', '', '',0);

  constructor(private updatecourseservice: UpdateCourseService, private _router: Router, private _route: ActivatedRoute) {
    this.course = new CourseList('', '', '', '', '', this.video[0], '');
  }

  ngOnInit() {
    this._route.params.subscribe((param: Params) => {
      this.categoryId = param['categoryId'];
      this.categoryname = param['categoryname'];
      this._id = param['courseId'];
      this.updatecourseservice.serviceFunction(this._id)
        .then(response => {
          this.course = response;
        });
    });
  }

  onSubmit() {
    let updateCourse = {
      "_id": this.course._id,
      "name": this.course.name,
      "description": this.course.description,
      "status": this.course.status
    }
    this.updatecourseservice.serviceUpdateFunction(updateCourse)
      .then(data => {
        this._router.navigate(['/courseList', this.categoryId, this.categoryname], { relativeTo: this._route, skipLocationChange: true });
      });
  }
}
