import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';

import { HttpInterceptor } from '../../../shared/httpInterceptor/httpInterceptor';
import { CourseList } from '../../../shared/models/courseList.model.js';

@Injectable()

export class UpdateCourseService {

  constructor(private _http: HttpInterceptor) { }

  serviceFunction(_id: any) {
    return this._http.get(`/api/courses/${_id}`)
      .toPromise()
      .then(response => response.json() as CourseList).catch(this.handleError);
  }

  serviceUpdateFunction(updateCourse: any) {
    return this._http.put('/api/updateCourse', updateCourse)
      .toPromise()
      .then(response => response.json()).catch(this.handleError);
  }
   private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
