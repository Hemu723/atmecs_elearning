import { Component, OnInit } from '@angular/core';
import { UsersQuizService } from './getAllUserQuiz.service';

@Component({
    selector: 'get-all-user-quiz',
    templateUrl: './getAllUserQuiz.view.html',
})

export class getAllUserQuizComponent implements OnInit {

    quizresult: any;
    term: string;
    quizdata: any = null;

    constructor(private _UserQuizService: UsersQuizService) { }

    ngOnInit() {
        this._UserQuizService.getAllUserQuiz()
            .then((Response:any) => {
                this.quizresult = Response;
            });
    }

    getQuizResult(i: any) {
        this.quizdata = this.quizresult[i].quiz;
        this.quizdata = Array.from(this.quizdata);
    }


}