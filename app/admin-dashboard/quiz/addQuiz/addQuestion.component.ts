import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { addQuestionService } from './addquestion.service';

@Component({
  selector: 'app-quiz',
  templateUrl: './addQuestion.view.html'
})

export class CreateQuizQuestionComponent {
  courseId: string;
  videolist: any;

  Question: string;
  opts: string[] = [];
  optionsField: string;
  answer: string = "select correct answer";
  point: number;
  note: string;
  videoId: string;

  option1:string;
  option2:string;
  option3:string;
  option4:string;
  option5:string;
  constructor(private _addquestionservice: addQuestionService,
    private _router: Router,
    private _activatedrouter: ActivatedRoute) {
  }

  ngOnInit() {
    console.log("inside constructor");
    this._activatedrouter.params.subscribe((params: Params) => {
      this.courseId = params['courseId'];
      this.videoId = params['videoId'];
      console.log(this.courseId);
      this._addquestionservice.getCourseVideoId(this.courseId)
        .then(res => {
          console.log(res);
          this.videolist = res;
        }
        );
    });
  }

  addQuizQuestion(_Question: string, _option1: string, _option2: string, _option3: string, _option4: string, _option5: string, _optionsField: string, _answer: string, _point: number, _note: string) {
    let quizQuestionObj = {
      "question": _Question,
      "options": {
        "opt1": _option1,
        "opt2": _option2,
        "opt3": _option3,
        "opt4": _option4,
        "opt5": _option5
      },
      "optionField": _optionsField,
      "answer": _answer,
      "point": _point,
      "note": _note,
      "videoId": this.videoId,
      "courseId": this.courseId
    }
    this._addquestionservice.addQuestion(quizQuestionObj).then(res => {
    this._router.navigate(['/quizQuestionsList'+'/'+this.courseId+'/'+this.videoId])
    });
  }
}
