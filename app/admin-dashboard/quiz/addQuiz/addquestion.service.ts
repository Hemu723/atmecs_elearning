import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';

import { HttpInterceptor } from '../../../shared/httpInterceptor/httpInterceptor';

@Injectable()

export class addQuestionService {

    constructor(private _http: HttpInterceptor) { }

    getCourseVideoId(courseId: string) {
        return this._http.get(`/api/getVideoByCourseId/${courseId}`)
            .toPromise().then(response => response.json().result.video)
            .catch(this.handleError);
    }

    addQuestion(quizQuestionObj: any) {
        return this._http.post(`/api/addQuestion`, quizQuestionObj)
            .toPromise().then(response => response.json().result)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
