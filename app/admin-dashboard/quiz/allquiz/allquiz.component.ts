import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { quizService } from './allquiz.service';
import { Test } from '../../../shared/models/Test.model';

@Component({
    selector: 'app-allquiz',
    templateUrl: './allquiz.view.html',
    styleUrls: ['./allquiz.view.css']
})

export class AllQuizComponent implements OnInit {
    courseId: string;
    videoId: string;
    categoryId: string;
    test: Test[] = [];
    categoryName: string;
    courseName: string;
    courselistitem_Id: string;
    videoName: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private _quizservice: quizService,
    ) {
    }

    ngOnInit() {
        console.log("inside constructor");
        this.route.params.subscribe((params: Params) => {
            this.courseId = params['courseId'];
            this.videoId = params['videoId'];
            this.courselistitem_Id = localStorage.getItem('courselistitem_Id');
            this.categoryName = localStorage.getItem('category');
            this.courseName = localStorage.getItem('selectedCourses');
            this.categoryId = localStorage.getItem('categoryid');
            this.videoName = localStorage.getItem('videoName');
            this._quizservice.getQuizQuestions(this.videoId)
                .then(res => {
                    this.test = res;
                }
                );
        });
    }

    deleteQuestion(Questionid: string) {
        this._quizservice.deleteQuestionService(Questionid);
        for (let i = 0; i < this.test.length; i++) {
            if (this.test[i]._id === Questionid) {
                this.test.splice(i, 1);
                break;
            }
        }
    }
}





