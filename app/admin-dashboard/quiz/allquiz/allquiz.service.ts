import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';

import { HttpInterceptor } from '../../../shared/httpInterceptor/httpInterceptor';
import { Test } from '../../../shared/models/test.model';

@Injectable()

export class quizService {

    constructor(private _http: HttpInterceptor) { }

    getQuizQuestions(videoId: string) {
        return this._http.get(`/api/getQuestionsByVideoId/${videoId}`)
            .toPromise().then(response => response.json().result as Test[])
            .catch(this.handleError);
    }

    deleteQuestionService(Questionid: string) {
        return this._http.delete(`/api/deleteQuestion/${Questionid}`)
            .toPromise().then(res => res)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
