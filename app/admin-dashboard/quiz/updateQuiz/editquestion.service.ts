import { Injectable } from '@angular/core';
import { HttpInterceptor } from '../../../shared/httpInterceptor/httpInterceptor';
import 'rxjs/add/operator/toPromise';

import { Test } from '../../../shared/models/test.model';

@Injectable()

export class EditQuestionService {

    constructor(private _http: HttpInterceptor) { }

    quizUpdatequestion(requestObj: any) {
        return this._http.put(`/api/UpdateQuestion`, requestObj)
            .toPromise().then(response => response.json())
            .catch(this.handleError);
    }

    getQuestionById(questionId: string) {
        return this._http.get(`/api/getQuestionById/${questionId}`)
            .toPromise().then(response => response.json().result as Test)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
