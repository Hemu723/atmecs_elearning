import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { EditQuestionService } from './editquestion.service';
import { Test } from '../../../shared/models/test.model';

@Component({
    selector: 'app-editquiz',
    templateUrl: './editquiz.view.html'
})

export class EditQuizQuestionComponent {
    test: Test;
    Question: string;
    options: string[];
    optionsField: string;
    answer: string;
    point: string;
    note: string;
    status: boolean;
    courseId: string;
    videoId: string;
    questionId: string;

    constructor(private _editquestionservice: EditQuestionService,
        private _router: Router,
        private route: ActivatedRoute, ) {
    }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.courseId = params['courseId'];
            this.videoId = params['videoId'];
            this.questionId = params['_id'];
            this._editquestionservice.getQuestionById(this.questionId)
                .then(res => {
                    this.test = res;
                }
                );
        });
    }

    updateQuestion(_Question: string, _option1: string, _option2: string, _option3: string, _option4: string, _option5: string, _optionsField: string, _answer: string, _point: string, _note: string) {
        let requestObj = {
            "id": this.questionId,
            "question": _Question,
            "options": {
                "opt1": _option1,
                "opt2": _option2,
                "opt3": _option3,
                "opt4": _option4,
                "opt5": _option5
            },
            "optionField": _optionsField,
            "answer": _answer,
            "point": _point,
            "note": _note,
            "videoId": this.videoId,
            "courseId": this.courseId
        }
        this._editquestionservice.quizUpdatequestion(requestObj);
        this._router.navigate(['/quizQuestionsList', this.courseId, this.videoId]);
    }

}
