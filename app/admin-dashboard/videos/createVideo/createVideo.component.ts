import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { FileUploader } from 'ng2-file-upload';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { FormsModule } from '@angular/forms'
import { FormBuilder, Validators } from '@angular/forms';

import { VideoDescription } from "../../../shared/models/videoDesc.model";
declare var swal: any;

@Component({
    selector: '',
    templateUrl: './createVideo.view.html'
})

export class CreateVideoComponent {
    categoryId: string;
    courseId: string;
    video: Array<VideoDescription> = [];
    videoPath: string[] = [];
    videoName: string;
    videoDescription:string;
    uploader: FileUploader;

   constructor(private _router: Router,
        private route: ActivatedRoute) {
        this.uploader = new FileUploader({ url: `/api/uploadVideoFile/` });
    }

   ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.categoryId = params['categoryId'];
            this.courseId = params['courseId'];
            console.log(this.videoName);
        });
    }

   addVideo() {
        swal({
            text: 'Please wait',
            allowOutsideClick: false,
            allowEscapeKey: false,
        });
        swal.showLoading();
        this.uploader.options.url = `/api/uploadVideoFile/` + this.categoryId + `/` + this.courseId + `/` + this.videoName + `/` + this.videoDescription;
        this.uploader.queue[0].url = `/api/uploadVideoFile/` + this.categoryId + `/` + this.courseId + `/` + this.videoName + `/` + this.videoDescription;
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            swal.close();
            this._router.navigate(['/videoList', this.categoryId, this.courseId])
        };
        this.uploader.uploadAll();
    }
}