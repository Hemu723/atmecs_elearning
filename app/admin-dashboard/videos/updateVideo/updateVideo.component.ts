import { Component, OnInit } from '@angular/core';
import { UpdateVideoService } from './updateVideo.service.js';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';

import { VideoDescription } from '../../../shared/models/videoDesc.model';
import { DomSanitizer } from '@angular/platform-browser';
declare var swal: any;

@Component({
    selector: 'updateVideo',
    templateUrl: './updateVideo.view.html'
})

export class UpdateVideoComponent implements OnInit {
    videoId: String;
    categoryId: String;
    courseId: String;
    show: Boolean = false;
    uploader: FileUploader;
    video: VideoDescription;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private _updateVideoService: UpdateVideoService,
        private sanitizer: DomSanitizer) {
        this.uploader = new FileUploader({ url: `/api/uploadVideoFile/` });
        this.video = new VideoDescription("", "", "", "", "",0);
    }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.videoId = params['videoId'];
            this.categoryId = params['categoryId'];
            this.courseId = params['courseId'];
            this._updateVideoService.getvideoDetailsByVideoId(this.videoId, this.courseId).then((Response:any) => {
                this.video = Response.data;
            });
        });
    }

    updateVideo() {
        swal({
            text: 'Please wait',
            allowOutsideClick: false,
            allowEscapeKey: false,
        });
        swal.showLoading();
        if (this.show) {
            this.uploader.options.url = `/api/uploadVideoFile/` + this.categoryId + `/` + this.courseId + `/` + this.video.name + `/` + this.video.desc;
            this.uploader.queue[0].url = `/api/uploadVideoFile/` + this.categoryId + `/` + this.courseId + `/` + this.video.name + `/` + this.video.desc;
            this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
                swal.close();
                this.router.navigate(['/videoList', this.categoryId, this.courseId]);
            };
            this.uploader.uploadAll();
        } else {
            this.updatvideo();
        }
    }

    updatvideo() {
        let responseObject =
            {
                "courseId": this.courseId,
                "videoId": this.videoId,
                "video":
                {
                    "name": this.video.name,
                    "description": this.video.desc,
                    "path": this.video.path,
                    "status": this.video.status
                }
            }
        this._updateVideoService.UpdateVideoservice(responseObject)
            .then((res:any) => {
                swal.close();
                this.router.navigate(['/videoList', this.categoryId, this.courseId]);
            });
    }

    deleteVideobyId(videoId: string) {
        this._updateVideoService.deleteVideobyIdservice(videoId, this.video.path);
        this.video.path = "";
        this.show = true;
    }
}

