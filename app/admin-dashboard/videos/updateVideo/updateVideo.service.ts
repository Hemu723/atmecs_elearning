import { Injectable } from '@angular/core';
import { HttpInterceptor } from '../../../shared/httpInterceptor/httpInterceptor';
import 'rxjs/add/operator/toPromise';

import { VideoDescription } from '../../../shared/models/videoDesc.model';

@Injectable()

export class UpdateVideoService {

    constructor(private _http: HttpInterceptor) { }

    deleteVideobyIdservice(videoid: string, videopath: String) {
        return this._http.put(`/api/deleteVideoById`, { "videoId": videoid, "videoURL": videopath })
            .toPromise().then(response => response.json())
            .catch(this.handleError);
    }

    getvideoDetailsByVideoId(videoid: String, courseId: String) {
        return this._http.get(`/api/getVideoById/${videoid}/${courseId}`)
            .toPromise().then(response => response.json())
            .catch(this.handleError);
    }

    UpdateVideoservice(requestObject: any) {
        return this._http.put(`/api/updateVideo`, requestObject)
            .toPromise().then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}