import { Component, OnInit, ViewChild, AfterViewChecked } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

import { VideoDescription } from '../../../shared/models/videoDesc.model';
import { VideoService } from './videoList.service';
import { CourseList } from '../../../shared/models/courseList.model';
declare let $:any;
declare var swal: any;

@Component({
    templateUrl: './videoList.view.html'
})

export class VideoListComponent implements OnInit, AfterViewChecked {
    courseName: String;
    categoryId: string;
    courseId: String;
    videolink: string;
    videolist: VideoDescription[] = [];
    categoryName: string;
    videoName: string;
    videoDesc: string;

    constructor(private route: ActivatedRoute, private router: Router, private videoService: VideoService) {

    }

    ngOnInit() {
        this.route.params.subscribe((param: Params) => {
            this.categoryId = param['categoryId'];
            this.courseId = param['courseId'];
            this.getVideoByCourseId();
            this.categoryName = localStorage.getItem('category');
            this.courseName = localStorage.getItem('selectedCourses');
            localStorage.setItem('categoryid', this.categoryId);
        })
    }


    ngAfterViewChecked() {
        var length = this.videolist.length;
        $(document).ready(function(){
            var old;
            for(var i = 0; i < length; i++) {
                $("#priority"+i).focus(function(){
                    var row_index = $(this).parent().parent().index('tr') - 1;
                    $("#row"+row_index).addClass("swapcolor1");
                });
                $("#priority"+i).focusout(function(){
                    var row_index = $(this).parent().parent().index('tr') - 1;
                    $("#row"+row_index).removeClass("swapcolor1");
                });
                $("#priority"+i).keyup(function(){
                    var index = $(this).val();
                    var row_index = $(this).parent().parent().index('tr') - 1;
                    if(index-1 != row_index) {
                        if(this.old!=undefined && this.old!="") {
                            $("#row"+(this.old-1)).removeClass("swapcolor2");
                        }
                        if(index!=undefined) {
                            $("#row"+(index-1)).addClass("swapcolor2");
                        }
                        this.old = index;
                    }
                });
            }
        })
    }

    getVideoByCourseId() {
        swal({
            text: 'Please wait',
            allowOutsideClick: false,
            allowEscapeKey: false,
        });
        swal.showLoading();
        this.videoService.getVideoByCourseIdService(this.courseId).then((Response:any) => {
            this.videolist = Response;
            swal.close();
        });
    }

    deleteVideo(id: String, path: String) {
        this.videoService.deletingCourseService(id, path)
            .then((res:any) => {
                for (let i = 0; i < this.videolist.length; i++) {
                    console.log(id);
                    if (this.videolist[i]._id === id) {
                        this.videolist.splice(i, 1);
                        break;
                    }
                }
            });
    }

    addQuiz(_courseId: string, _id: string, _videoName: string) {
        this.videoName = _videoName;
        localStorage.setItem('videoName', this.videoName);
        this.router.navigate(['/quizQuestionsList', this.courseId, _id]);
    }

    swapVideo(videoId:string,oldPriority:string,currentPosition:number) {
        var swapPosition = Number((<HTMLInputElement>document.getElementById('priority'+currentPosition)).value);
        if (swapPosition != null && swapPosition > 0 && swapPosition - 1 != currentPosition && swapPosition <= this.videolist.length) {
            var data= {
                "courseId": this.courseId,
                "oldPriority":oldPriority,
                "videoId": videoId,
                "newPriority": this.videolist[swapPosition-1].priority
            };
            this.videoService.swapVideo(data).then((res:any) => {
                if (res.message == "SUCCESS") {
                    this.getVideoByCourseId();
                    swal({title: 'Priority is successfully changed',timer:2000});
                }
                else {
                    swal({title: 'Failed to change priority',type: 'warning'});
                }
            });
        } else {
            swal({title: 'Please enter valid entry',type: 'warning'});
        }
    }

    modalOpen(index: number) {
        this.videolink = this.videolist[index].path;
        this.videoName = this.videolist[index].name;
        this.videoDesc = this.videolist[index].desc;
        this.modal.open('lg');
    }

    @ViewChild('modal')
    modal: ModalComponent;
    ngAfterViewInit() {
    }

    close(event: any) {
        this.videolink = "";
        this.modal.close();
    }

    open() {
        this.modal.open();
    }

    dismiss() {
        this.modal.dismiss();
    }
}
