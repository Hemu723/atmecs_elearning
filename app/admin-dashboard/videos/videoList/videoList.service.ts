import { Injectable } from '@angular/core';
import { HttpInterceptor } from '../../../shared/httpInterceptor/httpInterceptor';
import 'rxjs/add/operator/toPromise';

import { VideoDescription } from '../../../shared/models/videoDesc.model';

@Injectable()

export class VideoService {

  constructor(private _http: HttpInterceptor) { }

  getVideoByCourseIdService(courseID: String) {
    return this._http.get(`/api/getVideoByCourseId/${courseID}`)
      .toPromise().then(response => response.json().result.video as VideoDescription[]).catch(this.handleError);
  }

  deletingCourseService(id: String, path: String) {
    return this._http.put('/api/deleteVideoById', { "videoId": id, "videoURL": path })
      .toPromise().then(response => response.json()).catch(this.handleError);
  }

  swapVideo(data:any) {
     return this._http.put("/api/updateVideoPriority", data)
     .toPromise().then(response => response.json()).catch(this.handleError);
  }

   private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}