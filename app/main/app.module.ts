import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'ngx-accordion';
import { Ng2FileRequiredModule } from 'ng2-file-required';

import {VgCoreModule} from 'videogular2/core';
import {VgControlsModule} from 'videogular2/controls';
import {VgOverlayPlayModule} from 'videogular2/overlay-play';
import {VgBufferingModule} from 'videogular2/buffering';

import { AppComponent } from './appComponent/app.component';
import { SuperAdminRoutingModule,superAdminComponents } from '../super-admin-dashboard/superAdmin-routes/superAdmin.routing.module';
import { AdminRoutingModule,adminComponents } from '../admin-dashboard/admin-routes/admin.routing.module';
import { UserRoutingModule,userComponents } from '../user-dashboard/user-routes/user.routing.module';
import { SharedRoutingModule,sharedComponents } from '../shared/shared-routes/shared.routing.module';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule, // <-- import the FormsModule before binding with [(ngModel)]
    HttpModule,
    Ng2Bs3ModalModule,
    AccordionModule,
    Ng2FileRequiredModule,
    VgCoreModule, VgControlsModule, VgOverlayPlayModule, VgBufferingModule,
    SuperAdminRoutingModule,
    AdminRoutingModule,
    UserRoutingModule,
    SharedRoutingModule
  ],
  declarations: [
    AppComponent,superAdminComponents,adminComponents,userComponents,sharedComponents
  ],
 bootstrap: [AppComponent]
})
export class AppModule { }
