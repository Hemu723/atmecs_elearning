import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    templateUrl: './app.view.html',
})
export class AppComponent {
    func() {
        if (localStorage.getItem("isLogined") == "true")
            return true;
        return false;
    }
}