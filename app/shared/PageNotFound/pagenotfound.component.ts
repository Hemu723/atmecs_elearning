import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'app-page-not-found',
    templateUrl: './page.notfound.view.html',
    styleUrls: ['./pagenotfound.view.css']
    
})

export class PageNotFoundComponent implements OnInit {

    
    home:any ="dashboard";

    ngOnInit(){
        if(localStorage.getItem('admin') != undefined)
        {
             this.home = "adminDashboardcategory";
        }
        if(JSON.parse(localStorage.getItem('userProfile')).role == "SUPERADMIN"){
            this.home = "superAdmin";
        }
      
       
    }
}
