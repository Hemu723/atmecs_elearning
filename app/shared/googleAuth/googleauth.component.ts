import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { UserLoginService } from '../login/userLogin/userLogin.service';

@Component({
    template: ``
})
export class GoogleAuth implements OnInit {

    constructor(private route: ActivatedRoute,
                private router: Router,
                private userLoginService: UserLoginService) {}

    ngOnInit() {
        this.route.params
        .subscribe((param: Params) => {
            this.getUserProfile(param.id);
        });
    }

    getUserProfile(userId: string) {
        this.userLoginService.getUserProfile(userId)
        .then(user => {
            console.log(user);
            localStorage.setItem('userProfile', JSON.stringify( user ));
            localStorage.setItem('isLogined', 'true');
            this.router.navigate(['/dashboard']);
        });
    }
}