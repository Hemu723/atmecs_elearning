import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';

import { JumbotronService } from '../jumbotron/jumbotron.service';

@Component({
    selector: 'app-jumbotron',
    templateUrl: './jumbotron.view.html'
})
export class JumbotronComponent implements OnInit {
    userName: string;
    genderImgUrl: string;
    userId: number;
    name: string;
    email: string;
    mobile: number;
    image: string;
    profile: any;
    valid: boolean = true;
    adminfeature: boolean = false;
    USER: boolean = false;
    ADMIN: boolean = false;
    SUPERADMIN: boolean = false;
    updatePassword:boolean=false;
    updatePasswordMessage:string;
    home:any ="dashboard";

    constructor(private router: Router, private _jumbotronService: JumbotronService) { }

    ngOnInit() {
        this.profile = JSON.parse(localStorage.getItem('userProfile'));
        if (localStorage.getItem("admin") == 'administrator'){
            this.home = "adminDashboardcategory";
            this.adminfeature = true;
            this.router.navigate(['/adminDashboardcategory']);
        } 
        if (this.profile.role == 'SUPERADMIN') {
            this.home = "superAdmin";
            this.SUPERADMIN = true;     
            this.router.navigate(['superAdmin']);
        } 
        if (this.profile.role == 'ADMIN') this.ADMIN = true;
        if (this.profile.role == 'USER') this.USER = true;
        this.userName = this.profile.name;
        if (this.profile.userProfile.image != '')
            this.genderImgUrl = JSON.parse(localStorage.getItem('userProfile')).userProfile.image;
        else if (this.profile.gender == 'Male')
            this.genderImgUrl = "../assets/img/male.png";
        else
            this.genderImgUrl = "../assets/img/female.png";
            
    }

    toggleStatus(status: boolean) {
        if (status === false) this.adminfeatures();
        else this.userFeatures();
    }

    adminfeatures() {
        localStorage.setItem("admin", "administrator");
        this.home = "adminDashboardcategory";
        this.adminfeature = true;
        this.router.navigate(['/adminDashboardcategory']);
    }
    userFeatures() {
        localStorage.removeItem("admin");
        this.home = "dashboard";
        this.adminfeature = false;
        this.router.navigate(['/dashboard']);
    }

    changePassword(_email: string, _oldPassword: string, _newPassword: string) {
        let changePwdObj = {
            "email": _email,
            "oldPass": _oldPassword,
            "newPass": _newPassword
        }
        this.updatePasswordMessage = "";    
        var a = 0;
        var b = 0;
        var c = 0;
        var passEx = new RegExp('(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}');
        this.updatePassword = true;
        if(_email == "" || _oldPassword == "" || _newPassword == "")
        {
            this.updatePasswordMessage = "<br> * Fill all the fields <br>";
            a = 1;

        }
        if(_email != JSON.parse(localStorage.getItem("userProfile")).email)
        {
            this.updatePasswordMessage = this.updatePasswordMessage+"<br> * Thats not your email ID!!! <br>";
            b = 1;

        }
        if(!passEx.test(_newPassword))
        {
            this.updatePasswordMessage = this.updatePasswordMessage+"<br> * Password must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters <br>";
            c = 1;
        }
        if(a == 0 && b == 0 && c == 0)
        { 
            this._jumbotronService.changePassword(changePwdObj).then(res => {
                if(res == false)
                {
                    this.updatePasswordMessage = "<br> Old Password is wrong <br>";
                }
                else{
                    this.updatePasswordMessage = "<br> Password changed successfully";
                    
                }
            });
            
            
        }
        
        

    }
    updatePasswordVariable(){
        this.updatePassword = false;
    }

    getUserProfile() {
        this.userId = this.profile._id;
        this._jumbotronService.getUserProfile(this.userId).then(Response => {
            this.profile = Response;
        });
    }

    editUserProfile(_mobile: number) {
        this.mobile = _mobile;
        this._jumbotronService.editUserProfile(this.mobile, this.userId);
    }

    logout() {
        localStorage.clear();
        this.router.navigate(['/login']);
    }
}