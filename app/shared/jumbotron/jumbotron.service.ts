import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()

export class JumbotronService {

    constructor(private http: Http) { }

    changePassword(changePwdObj: any): Promise<any> {
        return this.http.put('/api/changePassword', changePwdObj)
            .toPromise()
            .then(response => response.json().status)
            .catch(this.handleError);
            
    }

    getUserProfile(userId: any) {
        return this.http.get(`/api/getUserProfile/${userId}`)
            .toPromise()
            .then(response => response.json().result)
            .catch(this.handleError);
    }

    editUserProfile(_mobile: number, _userId: number) {
        return this.http.put('/api/updateUserProfile', { "mobile": _mobile, "userId": _userId })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}