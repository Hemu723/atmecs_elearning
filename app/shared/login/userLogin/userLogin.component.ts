import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { UserLoginService } from '../userLogin/userLogin.service';

@Component({
    selector: 'app-login',
    templateUrl: './userLogin.view.html'
})

export class UserLoginComponent implements OnInit, OnDestroy {
    imgarray: Array<String>;
    bgImg: String;
    bgImgNumber: number;
    setint: any;
    SecurityQuestion: any;
    value: boolean = true;
    profile:any = null;
    username:string;
    password:string;
    wrongCredentials:boolean = false;

    constructor(private loginService: UserLoginService,
        private router: Router) { }

    ngOnInit() {
        this.profile = JSON.parse(localStorage.getItem('userProfile'));
		localStorage.removeItem("admin");
        if(this.profile != null && this.profile.role == 'SUPERADMIN')      
            this.router.navigate(['superAdmin']);
        if(this.profile != null || this.profile != undefined)
            this.router.navigate(['dashboard']);
        this.imgarray = [
            'url("assets/img/login-bg.jpg")',
            'url("assets/img/login-bg-2.jpg")'
        ];
        this.bgImg = this.imgarray[0];
        this.bgImgNumber = 0;
        this.setint = setInterval(() => {
            if (this.bgImgNumber === 0) {
                this.bgImg = this.imgarray[1];
                this.bgImgNumber = 1;
            } else {
                this.bgImg = this.imgarray[0];
                this.bgImgNumber = 0;
            }
        }, 5000);
    }

    login(): void {
        this.loginService.login(this.username, this.password)
            .then(response => {
                if (response.status === true || response.message === 'SUCCESS') {
                    localStorage.clear();
                    localStorage.setItem('userProfile', JSON.stringify(response.result));
                    localStorage.setItem('isLogined', 'true');
                    if(response.result.role == 'SUPERADMIN')      
                        this.router.navigate(['superAdmin']);
                    else
                        this.router.navigate(['dashboard']);
                } else {
                    this.wrongCredentials = true;
                    setTimeout(() => {
                       
                        this.wrongCredentials=false;
                    }, 1000);
                    
                }
            });
    }
    
    forgotPassword(email: string) {
        let forgotPwd = { 'email': email }
        
        this.loginService.forgotPassword(forgotPwd).then(Response => {
            this.SecurityQuestion = Response;
            this.value = false;
        });
    }

    submitAnswer(email: string,answer1: string,answer2: string) {
        let responseObj =
            {
                "email": email,
                "answer1":answer1,
                "answer2": answer2
            }
       
        this.loginService.submitAnswer(responseObj);
    }

    ngOnDestroy() {
        if (this.setint) {
            clearInterval(this.setint);
        }
    }
}