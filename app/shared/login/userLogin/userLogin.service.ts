import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()

export class UserLoginService {

  constructor(private http: Http) { }

  getUserProfile(id: String): Promise<any> {
    return this.http.get(`api/getUserProfile/${id}`).toPromise()
      .then(response => response.json().result);
  }

  login(_username_: String, _password_: String): Promise<any> {

    return this.http.post('api/login', { username: _username_, password: _password_ })
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  forgotPassword(forgotPwd: any): Promise<any> {
    return this.http.put('/api/getSecurityQues', forgotPwd)
      .toPromise()
      .then(response => response.json().result)
      .catch(this.handleError);
  }

  submitAnswer(responseObj: any): Promise<any> {
    return this.http.put('/api/forgetPassword', responseObj)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}