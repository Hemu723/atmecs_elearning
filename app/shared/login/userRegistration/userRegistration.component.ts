import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../models/user.model';
import { UserProfile } from '../../models/userProfile.model';
import { UserRegistrationService } from '../userRegistration/userRegistration.service';

@Component({
    selector: 'app-user-registartion',
    templateUrl: './userRegistartion.view.html'
})
export class UserRegistrationComponent implements OnInit {
    role: string = "USER";
    name: String;
    email: String;
    user: User[];
    validEmail: boolean = true;
    validUser: boolean = true;
    validConfirmPassword = true;
    username: String;
    existingEmail: String;
    existingUsername: String;
    bgImg='url("assets/img/login-bg.jpg")';

    genderArray: Array<string> = [
        "Male",
        "Female"
    ];
    gender: string = this.genderArray[0];
    password: String;
    confirmpassword: String;
    mobile: number;

    question1Array: Array<string> = [
        "What is your favourite color?",
        "What is your favourite fruit?",
        "What is your favourite dish?",
        "What is your pet name?"
    ];
    question1: string = this.question1Array[0];
    answer1: string;

    question2Array: Array<string> = [
        "Where you were born?",
        "What is your favourite movie?",
        "What is your favourite book?",
        "What is your favourite brand?"
    ];
    question2: string = this.question2Array[0];
    answer2: string;

    successfull: boolean = false;

    constructor(private registerService: UserRegistrationService,
        private router: Router) {

    }

    ngOnInit() {}

    emailChange(EmailValue: String) {
        this.validEmail = true;
        this.existingEmail = "";
        this.registerService.validEmail(EmailValue)
                .then(res => (res == 0)?(this.validEmail = true):(this.validEmail = false));
        (this.validEmail)?(this.existingEmail = EmailValue):(this.existingEmail = "");

    }

    userChange(UserValue: String) {
        this.validUser = true;
        this.existingUsername = "";
        this.registerService.validUsername(UserValue)
                .then(res => (res == 0)?(this.validUser = true):(this.validUser = false));
        (this.validUser)?(this.existingUsername = UserValue):(this.existingUsername = "");

    }

    confirmPasswordChange(pwd: String) {
        this.validConfirmPassword = true;
        if (this.password != pwd) {
            this.validConfirmPassword = false;
        }
    }

    register(): void {

        let registerObj = {
            'name': this.name,
            'email': this.email,
            'username': this.username,
            'gender': this.gender,
            'role': this.role,
            'password': this.password,
            'mobile': this.mobile,
            'question1': {
                'q1': this.question1,
                'a1': this.answer1
            },
            'question2': {
                'q2': this.question2,
                'a2': this.answer2
            }
        };
        this.registerService.register(registerObj)
            .then(response => {
                setTimeout(() => {
                    this.successfull = true;


                }, 500);
                setTimeout(() => {
                    this.router.navigate(['/login']);
                }, 3500);

            });

    }
}