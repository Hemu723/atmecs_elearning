import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { User } from '../../models/user.model';
@Injectable()

export class UserRegistrationService {

    constructor(private http: Http) { }

    register(registerObj: any): Promise<any> {
        return this.http.post('api/register', registerObj)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    
     validEmail(_email:any):Promise<any>{
        return this.http.get(`/api/verifyEmail/${_email}`)
            .toPromise().then(response => response.json().result)
            .catch(this.handleError);
    }

    validUsername(_user:any):Promise<any>{
        return this.http.get(`/api/verifyUsername/${_user}`)
            .toPromise().then(response => response.json().result)
            .catch(this.handleError);
    }

     private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

   

}