export class Course {
    _id: string;
    name: string;
    description: string;
    imgPath: string;
    quotes: string;
    likeCount: number;
    likeFlag : boolean;
    status: string;
    courses : string[];
    comments : string[];

    constructor(_id: string, name: string, description: string, imgPath: string,
                quotes: string, likeCount: number, likeFlag : boolean,status: string) {
        this._id = _id;
        this.name = name;
        this.description = description;
        this.imgPath = imgPath;
        this.quotes = quotes;
        this.likeCount = likeCount;
        this.likeFlag  = likeFlag;
        this.status = status;
    }
}