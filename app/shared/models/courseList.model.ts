import { VideoDescription } from "../models/videoDesc.model.js";

export class CourseList {
   name: string;
   description: string;
   categoryId: string;
   categoryName: string;
   status: string;
   video : VideoDescription[];
   _id : string;

   constructor(name: string, description: string, categoryId: string, categoryName: string, status: string,video : VideoDescription[], _id : string ) {
        this.name = name;
        this.description = description;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.status = status;
        this._id = _id;
        this.video = video;
   }
}