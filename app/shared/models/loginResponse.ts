export class LoginResponse{
    _id : string;
    name : string;
    mobile : number;
    email : string;
    username : string;
    password : string;
    status : boolean;
    token : string; 
    userProfile : string;

    constructor(_id_ : string, _name_: string, _mobile_ :number , _email_ : string, _username_:string,
    _password_:string, _status_:boolean, _token_:string, _userProfile_:string){
        this._id = _id_;
        this.name = _name_;
        this.mobile = _mobile_;
        this.email = _email_;
        this.username = _username_;
        this.password = _password_;
        this.status = _status_;
        this.token = _token_;
        this.userProfile = _userProfile_;
    }
}