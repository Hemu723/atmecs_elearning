export class PrevTestResult {
    _id: String;
    totalMarks: Number;
    marksObtain: Number;
    courseId: String;
    categoryId: String;
    dot: String;
    constructor(_id: String, totalMarks: Number, marksObtain:Number,
                courseId: String, categoryId: String, dot: String)
   {
       this._id=_id;
       this.totalMarks=totalMarks;
       this.marksObtain=marksObtain;
       this.courseId=courseId;
       this.categoryId=categoryId;
       this.dot=dot;
   }
}