export class Test {
    _id: String;
    question: String;
    options: Array<String>;
    optionField: String;
    answer: String;
    point: Number;
    videoId: String;
    courseId: String;
    note: String;
    status: Boolean;
    random: String;

    constructor(_id: String, question: String, options: Array<String>,
                optionField: String, answer: String, point: Number, videoId: String, coursesId: String,
                note: String, status: Boolean, random: String)
   {
        this._id = _id;
        this.question = question;
        this.options = options;
        this.optionField = optionField;
        this.answer = answer;
        this.point = point;
        this.videoId = videoId;
        this.courseId = coursesId;
        this.note = note;
        this.status = status;
        this.random = random;
   }
}
   


