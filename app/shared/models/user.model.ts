import { UserProfile } from "../models/userProfile.model.js";

export class User {

    _id: string;
    name: string;
    email: string;
    username: string;
    role: string;
    status: string;
    userProfile: UserProfile;

    constructor(id: string, name: string, email: string, username: string, role: string, status: string, userProfile: UserProfile) {

        this._id = id;
        this.name = name;
        this.email = email;
        this.username = username;
        this.role = role;
        this.status = status;
        this.userProfile = userProfile;

    }
}