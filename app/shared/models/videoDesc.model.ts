export class VideoDescription {
    _id:string;
    priority:number;
    desc: string;
    name: string;
    path: string;
    status:string;
    constructor( _id_:string,_name_ : string, _desc_ : string, _path_ : string ,_status_:string,_prior_:number){
        this._id=_id_;
        this.name = _name_;
        this.desc = _desc_;
        this.path = _path_; 
        this.status=_status_;
        this.priority=_prior_;
    }
}