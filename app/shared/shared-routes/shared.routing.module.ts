import { NgModule } from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import { XHRBackend, RequestOptions } from '@angular/http';

import { AboutComponent } from '../about/about.component';
import { AppFooter } from '../appFooter/appFooter.component';
import { GoogleAuth } from '../googleAuth/googleauth.component';
import { JumbotronComponent } from '../jumbotron/jumbotron.component';
import { PageNotFoundComponent } from '../PageNotFound/pagenotfound.component';
import { UserLoginComponent } from '../login/userLogin/userLogin.component';
import { UserRegistrationComponent } from '../login/userRegistration/userRegistration.component';

import { JumbotronService } from '../jumbotron/jumbotron.service';
import { UserLoginService } from '../login/userLogin/userLogin.service';
import { UserRegistrationService } from '../login/userRegistration/userRegistration.service';

import { HttpInterceptor } from '../httpInterceptor/httpInterceptor';
import { AuthGuard } from '../../user-dashboard/authGuard/authguard.service';
import { SearchFilterPipe } from '../Pipes/search-filter.pipe';
import { FileSelectDirective, FileDropDirective } from 'ng2-file-upload';

const sharedRoutes: Routes = [
    {
        path: 'about',
        component: AboutComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'googleauth/:id',
        component: GoogleAuth
    },
    {
        path: 'register',
        component: UserRegistrationComponent
    },
    {
        path : 'login',
        component : UserLoginComponent
    },
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    },
    {
        path: '**',
        component: PageNotFoundComponent,
        canActivate: [AuthGuard]
    },
];


@NgModule({
    imports: [ RouterModule.forRoot(sharedRoutes) ],
    providers: [ 
        AuthGuard, JumbotronService, UserLoginService, UserRegistrationService,
        {
            provide: HttpInterceptor,
            useFactory:
            (backend: XHRBackend, defaultOptions: RequestOptions) => {
                return new HttpInterceptor(backend, defaultOptions);
            },
            deps: [XHRBackend, RequestOptions]
        }
     ],
    exports: [ RouterModule ]
})

export class SharedRoutingModule { }

export const sharedComponents = [
    AboutComponent, AppFooter, GoogleAuth,JumbotronComponent,PageNotFoundComponent,
    UserLoginComponent, UserRegistrationComponent, SearchFilterPipe,FileSelectDirective,FileDropDirective
];