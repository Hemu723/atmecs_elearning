import { NgModule } from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';

import { SuperAdminComponent } from '../superAdmin/superAdmin.component';

import { SuperAdminService } from '../superAdmin/superAdmin.service';

import { SuperAdminAuthGuard } from '../superAdminAuthGuard/superAdminAuthGuard.service';

const superAdminRoutes: Routes = [   
          { 
            path: 'superAdmin', 
            component: SuperAdminComponent, 
            canActivate: [SuperAdminAuthGuard] 
          }    
  ];

@NgModule({
  imports:  [ RouterModule.forRoot(superAdminRoutes) ],
  providers: [ SuperAdminAuthGuard, SuperAdminService ],
  exports:  [ RouterModule ]
})

export class SuperAdminRoutingModule { }

export const superAdminComponents = [ SuperAdminComponent ];

