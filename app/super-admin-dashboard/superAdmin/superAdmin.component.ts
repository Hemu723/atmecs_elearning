import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { SuperAdminService } from './superAdmin.service'
import { User } from '../../shared/models/user.model';
import { UserProfile } from '../../shared/models/userProfile.model';

@Component({
    selector: 'app-superAdmin',
    templateUrl: './superAdmin.view.html'

})

export class SuperAdminComponent implements OnInit {

    constructor(private router: Router, private superAdminService: SuperAdminService) { }

    oldRole: string;
    role: number;
    userId: string;
    oldStatus: string;
    status: number;
    user: User[];
    Admin: boolean = false;


    ngOnInit() {
        this.superAdminService.getAllUser()
            .then((res:any) => {
                this.user = res.result;
            }
            );
    }

    AssignRole(Role: string) {
        if (Role == undefined || Role == "USER") {
            return true
        }
        else
            return false;
    }

    AssignStatus(status: boolean) {
        if (status == true) {
            return false
        }
        else
            return true
    }

    makeAdmin(_role: string, userID: string) {
        if (_role) {
            this.role = 1;
        }
        this.superAdminService.makeAdmin(this.role, userID);
        this.Admin = true;
        this.ngOnInit();
    }

    makeUser(_role: string, userID: string) {
        if (_role) {
            this.role = 0;
        }
        this.superAdminService.makeUser(this.role, userID);
        this.Admin = false;
        this.ngOnInit();
    }

    activateUser(_status: boolean, userID: string) {
        if (!_status) {
            this.status = 1;
        }
        this.superAdminService.activateUser(this.status, userID);
        this.ngOnInit();
    }

    deactivateUser(_status: boolean, userID: string) {
        if (_status) {
            this.status = 0;
        }
        this.superAdminService.deactivateUser(this.status, userID);
        this.ngOnInit();
    }

    toggleRole(role: string, id: string) {
        if (role == "ADMIN") {
            this.makeUser(role, id);
        }
        else {
            this.makeAdmin(role, id);
        }
    }

    toggleStatus(status: boolean, id: string) {
        if (status == true) {
            this.deactivateUser(status, id);
        }
        else {
            this.activateUser(status, id);

        }
    }

}