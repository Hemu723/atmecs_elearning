import { Injectable } from '@angular/core';
import { HttpInterceptor } from '../../shared/httpInterceptor/httpInterceptor';
import 'rxjs/add/operator/toPromise';

import { User } from '../../shared/models/user.model';
import { UserProfile } from '../../shared/models/userProfile.model';

@Injectable()

export class SuperAdminService {

    constructor(private http: HttpInterceptor) { }

    getAllUser() {
        return this.http.get('/api/getAllUser')
            .toPromise().then(response => response.json() as User[])
            .catch(this.handleError);
    }

    makeAdmin(role: number, _userId: string) {
        return this.http.put(`/api/updateRole/${_userId}/${role}`, { "role": role, "userId": _userId })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    makeUser(role: number, _userId: string) {
        return this.http.put(`/api/updateRole/${_userId}/${role}`, { "role": role, "userId": _userId })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    activateUser(status: number, _userId: string) {
        return this.http.put(`/api/updateStatus/${_userId}/${status}`, { "status": status, "userId": _userId })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);

    }

    deactivateUser(status: number, _userId: string) {
        return this.http.put(`/api/updateStatus/${_userId}/${status}`, { "status": status, "userId": _userId })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

}
