import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable()

export class SuperAdminAuthGuard implements CanActivate {

    role: string;

    constructor(private router: Router) { }

    canActivate() {
        this.role = JSON.parse(localStorage.getItem('userProfile')).role;
        if (this.role == "SUPERADMIN") {
            return true;
        }

        else {
            console.log("sorry you dont have superadmin credentials");
            this.router.navigate(['/pageNotFound']);
            return false;
        }
    }

}