import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    isLogined: boolean;

    constructor(private router: Router) { }

    canActivate() {
        this.isLogined = JSON.parse(localStorage.getItem('isLogined'));
        if (this.isLogined) {
            return true;
        }

        else {
            this.router.navigate(['/login']);
            return false;
        }
    }

}