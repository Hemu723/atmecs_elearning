import { Component, OnInit, EventEmitter } from '@angular/core';
import { AccordionModule } from "ngx-accordion";
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CategoryListService } from './categoryList.service';

declare let $:any;

@Component({
    selector: 'app-category-List',
    templateUrl: './categoryList.view.html'
})

export class CategoryListComponent implements OnInit {
    list:any;
    categoryId:string;
    tog:number = 0;
    navText:string = "Open Navigation";
    
    constructor(private router: Router, private route:ActivatedRoute, private categoryListService:CategoryListService) {
    }
    
    ngOnInit() {
        this.categoryId = this.route.snapshot.params['id'];
        this.categoryListService.getDetailedCategories().then(res => this.list = res);
    }

    showCourses(categoryId: string) {
        this.router.navigate(['/classroom', categoryId, 'courses']);
    }

    showVideos(categoryId: string, courseId:string) {
         this.router.navigate(['/classroom', categoryId, 'video', courseId]);
    }

    showVideo(categoryId: string, courseId: string, videoId: string) {
        let urlRoute = '/classroom/' + categoryId + '/video';
        this.router.navigate([urlRoute, courseId, videoId]);
    }

    navToggle(){
       if($("#navDiv").hasClass("hide")){
            this.tog=1;
            this.navText = "Close Navigation";
            $("#navDiv").removeClass("hide");
            $("#navDiv").addClass("col-sm-10");
             this.categoryListService.newEvent(this.tog);
            return;
       }

       if($("#navDiv").hasClass("col-sm-10")){
           this.tog = 2;
           this.navText = "Open Navigation";
            $("#navDiv").removeClass("col-sm-10");
            $("#navDiv").addClass("hide");
            this.categoryListService.newEvent(this.tog); 
            return;
       }
        
    }
}