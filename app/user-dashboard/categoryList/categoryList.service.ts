import { Injectable } from '@angular/core';
import { HttpInterceptor } from '../../shared/httpInterceptor/httpInterceptor';
import  { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

@Injectable()
export class CategoryListService {

    private _subject = new Subject<any>();

    constructor(private http: HttpInterceptor) { }

    newEvent(event:any){
        this._subject.next(event);
    }

    get event$(){
        return this._subject.asObservable();
    }

     getDetailedCategories():Promise<any>{
        return this.http.get(`/api/getDetailedCategories`)
            .toPromise()
            .then(response => response.json().result as any)
            .catch(this.handleError);
    }
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
    
}