import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { CategoryListService } from '../categoryList/categoryList.service';

declare let $:any;

@Component({
    selector: 'classroom',
    templateUrl: './classRoom.view.html'
})
export class ClassRoom implements OnInit {

    constructor(private route: ActivatedRoute,
        private router: Router, private categoryListService:CategoryListService) {
    }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.router.navigate(['/classroom', params['id'], 'courses']);
        });
        this.categoryListService.event$.forEach(event => this.navToggle(event));
    }

    navToggle(event:any){
        if(event == 1){
            $("#nav1Div").removeClass("col-sm-1");
            $("#nav1Div").addClass("col-sm-3");
            $("#mainDiv").removeClass("col-sm-11");
            $("#mainDiv").addClass("col-sm-9");
            return;
        }
         if(event == 2){
            $("#nav1Div").removeClass("col-sm-3");
            $("#nav1Div").addClass("col-sm-1");
            $("#mainDiv").removeClass("col-sm-9");
            $("#mainDiv").addClass("col-sm-11");
            return;
        }
        
        

    }
}
