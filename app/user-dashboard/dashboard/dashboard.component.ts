import { Component, OnInit } from'@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

import { DashboardService } from './dashboard.service';
import { Course } from '../../shared/models/course.model';
declare let $:any;

@Component({
    selector: 'my-dashboard',
    templateUrl: './dashboard.view.html'
})
export class DashboardComponent implements OnInit {
    categories: Course[];
    userId:string;
    dislike : number;
    upVoteUrl : string;

    constructor(private dashboardService: DashboardService,
                private sanitizer:DomSanitizer) {
    }

    ngOnInit() {
        this.userId = JSON.parse(localStorage.getItem('userProfile'))._id;
        this.getAllCategories(this.userId);
        
        $(document).ready(function(){
            $('.container-fluid').perfectScrollbar({
                                                        wheelSpeed: 2,
                                                        wheelPropagation: true,
                                                        minScrollbarLength: 20
                                                    });
        });
    }

    getAllCategories(userId : string) {
        this.dashboardService.getAllCategories( userId ).then(categories => {
            for(let i=0; i<categories.length; i++) {
                categories[i].imgPath = 'url(' + categories[i].imgPath + ')' ;
                this.sanitizer.bypassSecurityTrustUrl(categories[i].imgPath.toString());
            }
            this.categories = categories;
        });
    }

    increase(categoryId : string, index:number) : void{ 
        this.dashboardService.counter(categoryId, this.userId)
        .then(response => {
            if(response.message == "ADDED"){
                this.categories[index].likeFlag = true;
                this.categories[index].likeCount++;
            }else{
                this.categories[index].likeFlag = false;
                this.categories[index].likeCount--;
            }
        });
    }
}