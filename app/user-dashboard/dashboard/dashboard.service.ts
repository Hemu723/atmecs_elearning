import { Injectable } from '@angular/core';
import { HttpInterceptor } from '../../shared/httpInterceptor/httpInterceptor';
import 'rxjs/add/operator/toPromise';

import { Course } from '../../shared/models/course.model';

@Injectable()

export class DashboardService {

    constructor(private http: HttpInterceptor) { }

    getAllCategories(_userId_: string): Promise<Course[]> {
        return this.http.get(`/api/getAllCategories/${_userId_}`)
            .toPromise()
            .then((response:any) => response.json() as Course[])
            .catch(this.handleError);
    }

    counter(_categoryId_: string, _userId_: string): Promise<any> {
        return this.http.get(`/api/likeCall/${_categoryId_}/${_userId_}`)
        .toPromise()
        .then((response:any) => response.json())
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
