import { Component, OnInit } from '@angular/core';

import { PreviousTestScoresService } from './previoustestscores.service';
import { PrevTestResult } from '../../shared/models/previousTestResult.model';

@Component({
    selector: 'previoustestresult',
    templateUrl: './previousetestscores.view.html'
})

export class PreviousTestScores implements OnInit {
    userId: string;
    prevtestresult: PrevTestResult[];

    constructor(private _previousTestScoresService: PreviousTestScoresService) {
    }

    ngOnInit() {
        this.userId = JSON.parse(localStorage.getItem('userProfile'))._id;
        this.prevTestResults();
    }

    prevTestResults() {
        this._previousTestScoresService.previousTestScoresService(this.userId).then((Response:any) => {
            this.prevtestresult = Response;
        });
    }

}
