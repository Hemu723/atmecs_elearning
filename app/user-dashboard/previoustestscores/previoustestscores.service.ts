import { Injectable } from '@angular/core';
import { HttpInterceptor } from '../../shared/httpInterceptor/httpInterceptor';

import { PrevTestResult } from '../../shared/models/previousTestResult.model';

@Injectable()

export class PreviousTestScoresService {

  constructor(private _http: HttpInterceptor) { }

  previousTestScoresService(_userId_: string) {
    return this._http.get(`/api/getQuizResult/${_userId_}`)
      .toPromise()
      .then(response =>
        response.json().result.quiz as PrevTestResult[])
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}