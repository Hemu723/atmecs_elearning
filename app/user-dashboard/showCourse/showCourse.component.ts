import { Component, OnInit } from '@angular/core';

import { ShowCourseService } from './showCourse.service';
import { CourseList } from '../../shared/models/courseList.model';
import { ActivatedRoute, Params, Router} from '@angular/router';

@Component({
    selector: 'show-course',
    templateUrl: './showCourse.view.html',

})

export class ShowCourseComponent implements OnInit {
    categoryId:string;
    courseList: CourseList[] = [];  

    constructor(private route: ActivatedRoute, private router:Router, private showCourseService: ShowCourseService) { }
        
    ngOnInit() {
       this.route.parent.params.subscribe((params: Params) => {
             this.categoryId = params['id'];                        
            this.showCourseService.getCourseList(this.categoryId)
                                    .then(response=>this.courseList=response); 
        });
    }  

    
    showVideo(courseId:string) {
         this.router.navigate(['/classroom', this.categoryId, 'video', courseId]);
    }  
}