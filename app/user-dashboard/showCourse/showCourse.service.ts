import { Injectable } from '@angular/core';
import { HttpInterceptor } from '../../shared/httpInterceptor/httpInterceptor';
import 'rxjs/add/operator/toPromise';
import { CourseList } from '../../shared/models/courseList.model';

@Injectable()
export class ShowCourseService {

    constructor(private http: HttpInterceptor) { }

    getCourseList(categoryId: string): Promise<CourseList[]> {
        return this.http.get(`/api/category/${categoryId}`)
            .toPromise()
            .then(response => response.json().result as CourseList[])
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}