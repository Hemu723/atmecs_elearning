import { Component, OnInit, Input } from '@angular/core';

import { ShowVideoService } from './showVideo.service';
import { ActivatedRoute, Params, Router} from '@angular/router';
import { VideoDescription } from '../../shared/models/videoDesc.model';

@Component({
    selector: 'show-video',
    templateUrl: './showVideo.view.html',

})

export class ShowVideoComponent implements OnInit {
    categoryId:string;
    courseId: string;
    videoList : VideoDescription[] = [];

    constructor(private route: ActivatedRoute, private router:Router, private showVideoService: ShowVideoService) { 
            
        }
        
    ngOnInit() {
       this.route.params.subscribe((params: Params) => {
            this.categoryId = this.route.snapshot.parent.params['id'];
            this.courseId = params['courseId'];
            this.showVideoService.getVideoList(this.courseId)
                                    .then(response=>this.videoList=response);               
        });
    }    

    showVideo(videoId:string) {       
        let urlRoute = '/classroom/' + this.categoryId + '/video';
        this.router.navigate([urlRoute, this.courseId, videoId]);
    }
}