import { Injectable } from '@angular/core';
import { HttpInterceptor } from '../../shared/httpInterceptor/httpInterceptor';
import { VideoDescription } from '../../shared/models/videoDesc.model';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ShowVideoService {

    constructor(private http: HttpInterceptor) { }
    
    getVideoList(courseId:string): Promise<VideoDescription[]> {
        return this.http.get(`/api/getVideoByCourseId/${courseId}`)
            .toPromise()
            .then(response => response.json().result.video as VideoDescription[])
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}