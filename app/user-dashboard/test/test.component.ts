import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TestService } from './test.service';
import { Test } from '../../shared/models/test.model';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
    selector: 'app-test',
    templateUrl: './test.view.html',
})
export class TestComponent implements OnInit {
    categoryId: string;
    timeInMin = 2;
    viewQue: Test;
    currentIndex: number = 0;
    videoId: string;
    courseId: string;
    answerList: any;
    totalQuestion: number;
    totalMarkstopass: number;

    @Output() submitExam = new EventEmitter<any>();

    @Input() test: Test[];

    constructor(private router: Router,
        private testService: TestService,
        private route: ActivatedRoute) {
        this.answerList = [];
    }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.courseId = params['courseId'];
            this.videoId = params['videoId'];
            this.categoryId = this.route.snapshot.parent.params['id'];
            this.getTestquestion(this.videoId);
        });
    }

    getTestquestion(videoId: string) {
        this.testService.getTestquestion(videoId)
            .then(queList => {
                this.test = queList;
                this.viewQue = this.test[0];
                this.totalQuestion = queList.length;
            }, error => { console.log("error occured"); });
    }

    checkAnswer(position: any, options: any) {
        var jobj = {
            "q_id": this.viewQue._id,
            "submit_opt": options
        }
        this.viewQue.answer = options;
        this.test[this.currentIndex].answer = options;
        this.validate(jobj);
    }

    validate(jobj: any) {
            for (let j = 0; j < this.answerList.length; j++) {
                if (this.answerList[j].q_id === jobj.q_id) {
                        this.answerList.splice(j, 1);
                        break;
                }
            }
            this.answerList.push(jobj);
    }

    previousQuestion() {
        if (this.currentIndex != 0) {
            this.currentIndex--;
        }
        this.viewQue = this.test[this.currentIndex];
    }

    nextQuestion() {
        if (this.currentIndex < this.totalQuestion - 1) {
            this.currentIndex++;
            this.viewQue = this.test[this.currentIndex];
        }
    }

    onTimeExpire() {
        alert("Exam Time is Over");
        this.submitTest();
    }

    submitTest() {
        const allQ = this.test;
        var jsonData = {
            "userId": JSON.parse(localStorage.getItem('userProfile'))._id,
            "categoryId": this.categoryId,
            "courseId": this.courseId,
            "quiz_resp": this.answerList
        };
        this.testService.postQuizAnswer(jsonData).then(jobj => {
            this.totalMarkstopass = jobj.totalMarks;
            delete jobj.message;
            delete jobj.status;
            localStorage.removeItem('testResultData');
            localStorage.setItem('testResultData', JSON.stringify(jobj))
            let urlRoute = '/classroom/' + this.categoryId + '/testResult';
            this.router.navigate([urlRoute, this.courseId, this.videoId]);
            this.submitExam.emit(this.test);
        },
            error => { console.log("error occured"); }
        );
    }
}
