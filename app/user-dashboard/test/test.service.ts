import { Injectable } from '@angular/core';
import { HttpInterceptor } from '../../shared/httpInterceptor/httpInterceptor';
import 'rxjs/add/operator/toPromise';

import { Test } from '../../shared/models/test.model.js';

@Injectable()

export class TestService {

    constructor(private http: HttpInterceptor) { }

    getTestquestion(videoId: string): Promise<Test[]> {
        return this.http.get(`/api/getRandomQuestion/${videoId}`)
            .toPromise()
            .then((response:any) => response.json().result as Test[])
            .catch(this.handleError);
    }

    postQuizAnswer(jsonData: any): Promise<any> {
        return this.http
            .post('/api/submitQuiz', jsonData)
            .toPromise()
            .then((response:any) => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
