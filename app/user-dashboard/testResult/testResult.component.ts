import { OnInit, OnDestroy, Component } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'app-testResult',
    templateUrl: './testResult.view.html'
})

export class TestResultComponent implements OnInit {
    testResultData: string;
    obtainScore: number;
    totalScore: number;

    categoryId:string;
    courseId:string;
    videoId:string;

    constructor(private router: Router,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.courseId = params['courseId'];
            this.videoId = params['videoId'];
            this.categoryId = this.route.snapshot.parent.params['id'];
        });
        this.testResultData = JSON.parse(localStorage.getItem('testResultData'));
        this.obtainScore = JSON.parse(localStorage.getItem('testResultData')).markObtain;
        this.totalScore = JSON.parse(localStorage.getItem('testResultData')).totalMarks;
    }

    backToVideo() {
        let urlRoute = '/classroom/' + this.categoryId + '/video/';
        this.router.navigate([urlRoute, this.courseId, this.videoId]);
    }
}

