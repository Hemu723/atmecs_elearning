import { Component, OnInit, Input, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { TestComponent } from "../test/test.component";

@Component({
  selector: 'timer',
  templateUrl: './timer.html'
})

export class TimerComponent implements OnInit, AfterViewInit {

      @Input() timeInMinutes: any;
      @Output() onTimeExpire = new EventEmitter();

      constructor() { }

      ngOnInit() {  }

      ngAfterViewInit() {
          var countDownDate = new Date(new Date(new Date().setMinutes(new Date().getMinutes() + this.timeInMinutes))).getTime();
          const evt = this.onTimeExpire;
          var x = setInterval(function() {
                                        var now = new Date().getTime();
                                        var distance = countDownDate - now;
                                        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                                        if(document.getElementById("examTimer")) {

                                          document.getElementById("examTimer").innerHTML = " "+ minutes + " Minute " + seconds + " Second ";
                                        }

                                        

                                        if (distance < 0) {
                                            clearInterval(x);
                                            if(document.getElementById("examTimer")) {
                                            document.getElementById("examTimer").innerHTML = "EXPIRED"; }
                                            evt.emit();
                                        }
                    }, 1000);
      }


}