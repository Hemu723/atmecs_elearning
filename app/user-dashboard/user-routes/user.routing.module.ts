import { NgModule } from '@angular/core';
import { RouterModule,Routes} from '@angular/router';

//Components
import { ClassRoom } from '../classRoom/classRoom.component';
import { ShowCourseComponent } from '../showCourse/showCourse.component';
import { ShowVideoComponent } from '../showVideo/showVideo.component';
import { VideoPlay } from '../videoComponent/video.component';
import { TestComponent } from '../test/test.component';
import { QuotesDesc } from '../quotesDescription/quotesDesc.component';
import { TestResultComponent } from '../testResult/testResult.component';
import { PreviousTestScores } from '../previoustestScores/previoustestscores.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { TimerComponent } from '../timercomponent/timer';
import { CategoryListComponent } from '../categoryList/categoryList.component';

//Services
import { ShowCourseService } from '../showCourse/showCourse.service';
import { ShowVideoService } from '../showVideo/showVideo.service';
import { TestService } from '../test/test.service';
import { VideoComponentService } from '../videoComponent/video.service';
import { PreviousTestScoresService } from '../previoustestScores/previoustestscores.service';
import { DashboardService } from '../dashboard/dashboard.service';
import { CategoryListService } from '../categoryList/categoryList.service';

import { AuthGuard } from '../authGuard/authguard.service';


const userRoutes:Routes = [

    {
        path: 'classroom/:id',
        component: ClassRoom,
        canActivate: [AuthGuard],
        children: [
          {
            path: '',
            redirectTo: 'quotes',
            pathMatch: 'full',
          }, 
          {
            path:'courses',
            component: ShowCourseComponent,
          },
          {
            path:'video/:courseId',
            component: ShowVideoComponent,
          },
          {
            path: 'video/:courseId/:videoId',
            component: VideoPlay,
          },
          {
            path: 'test/:courseId/:videoId',
            component: TestComponent
          },
          {
            path: 'quotes',
            component: QuotesDesc
          },
          {
            path: 'testResult/:courseId/:videoId',
            component: TestResultComponent
          }
        ]
      },
      {
        path: 'dashboard',
        canActivate: [AuthGuard],
        component: DashboardComponent
      },
      {
        path: 'previousTestScores',
        component: PreviousTestScores,
        canActivate: [AuthGuard]
      }

];

@NgModule({
    imports: [
        RouterModule.forRoot(userRoutes)
    ],
    providers:[
      AuthGuard,ShowCourseService,ShowVideoService,TestService,VideoComponentService,
      PreviousTestScoresService,DashboardService,CategoryListService
    ],
    exports : [ RouterModule ]
    
})
export class UserRoutingModule {}

export const userComponents:any = [
  ClassRoom, ShowCourseComponent, ShowVideoComponent, VideoPlay, TestComponent, 
  QuotesDesc, TestResultComponent, PreviousTestScores, DashboardComponent, 
  TimerComponent, CategoryListComponent
];