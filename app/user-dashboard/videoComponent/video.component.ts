import { Component, OnInit, DoCheck, AfterViewChecked, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { VideoDescription } from '../../shared/models/videoDesc.model'
import { VideoComponentService } from './video.service';
import { ClassRoom } from '../classRoom/classRoom.component';
import { TestService } from '../test/test.service';
declare let $:any;
declare var swal: any;

@Component({
    selector: 'app-video-play',
    templateUrl: './video.component.view.html'
})
export class VideoPlay implements OnInit, DoCheck, AfterViewChecked, OnDestroy {
    videoUrl: string;
    videoName:string;
    videoList: VideoDescription[];
    userId: string;
    courseId: string;
    categoryId: string;
    duration: number;
    currentVideoId: string;
    currentVideoIndex: number;

    prevVideo: VideoDescription;
    nextVideo: VideoDescription;

    toggleMode = "Lights off";
    swalFlag:boolean = true;
    swalResumeFlag:boolean = true;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private videocomponentservice: VideoComponentService,
        private testService: TestService) {
    }

    ngOnInit() {
        this.userId = JSON.parse(localStorage.getItem('userProfile'))._id;
        this.route.params.subscribe((params: Params) => {
            this.courseId = params['courseId'];
            this.currentVideoId = params['videoId'];
            this.categoryId = this.route.snapshot.parent.params['id'];
            this.initialVideo();
        });
    }

    initialVideo() {
        this.videocomponentservice.getvideoList(this.courseId).subscribe(videoList => {
            this.videoList = videoList;

            if(this.videoList != undefined && this.videoList != null) {
                this.selectIndexVideoFromList();
                if(this.currentVideoIndex != undefined)     this.videoUrls(this.videoList[this.currentVideoIndex]);
                else this.videoUrls(this.videoList[0]);
            }
        });
    }

    selectIndexVideoFromList() {
        for (let i = 0; i < this.videoList.length; i++) {
            if (this.videoList[i]._id == this.currentVideoId) {
                this.currentVideoIndex = i;
            }
        }
    }

    getVideoDuration() {
         this.videocomponentservice.getDurationOfVideo(this.userId, this.currentVideoId)
                .then(currentTimeOfVideo => this.duration = currentTimeOfVideo );
    }

    videoUrls(video:VideoDescription) {
        if(video != null) {
            this.pauseVideo();
            this.videoName = video.name;
            this.currentVideoId = video._id;
            this.videoUrl = '/api/getClasroomVideo/' + this.userId + '/' + this.courseId + '/' + this.currentVideoId;
            this.getVideoDuration();
            this.prevVideo = this.prevVideosearch();
            this.nextVideo = this.nextVideoSearch();

            this.swalFlag = true;
            this.swalResumeFlag = true;
        }
    }

    prevVideosearch() {
        this.selectIndexVideoFromList(); 
        if (this.videoList[this.currentVideoIndex - 1] == undefined || this.videoList[this.currentVideoIndex - 1] == null)    return null;
        else    return this.videoList[this.currentVideoIndex - 1];
    }

    nextVideoSearch() {
        this.selectIndexVideoFromList();
        if (this.videoList[this.currentVideoIndex + 1] == undefined || this.videoList[this.currentVideoIndex + 1] == null)    return null;
        else    return this.videoList[this.currentVideoIndex + 1];
    }

    ngAfterViewChecked() {
        let video = <HTMLVideoElement>document.getElementById("app-video");
        if (video.readyState == 1 && this.duration > 0 && this.duration != video.duration && this.swalResumeFlag) {
            this.swalResumeFlag = false;
            swal({
                title: this.videoName,
                text: "Do you wish to resume from where you stopped?",
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonText: "RESUME",
                cancelButtonText: "Start Over"
            }).then( () => {
                        video.currentTime = this.duration;
                        this.playVideo();
                    }, () => { 
                        video.currentTime = 0;
                        this.playVideo();
                    });
        }
        if(this.duration == video.duration && this.swalResumeFlag) {
            this.swalResumeFlag = false;
            this.playVideo();
        } 
    }

    ngDoCheck() {
        let video = <HTMLVideoElement>document.getElementById("app-video");

        if(video.currentTime > this.duration && video.paused || video.currentTime > this.duration && video.ended) {
            let reqObj =
                {
                    "categoryId": this.categoryId,
                    "courseId": this.courseId,
                    "userId": this.userId,
                    "videoId": this.currentVideoId,
                    "duration": video.currentTime
                }
            this.duration = video.currentTime;
            this.videocomponentservice.sendvideotimeservice(reqObj); 
        }
    	if(this.swalFlag && video.ended) {
            this.swalFlag = false;
            this.testService.getTestquestion(this.currentVideoId).then(queList => {
                if(queList.length!=0) {
                    swal({
                        title: "Quiz",
                        text: "Video is Completed. Now Take Quiz",
                        type: "success",
                        showCloseButton: true,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        confirmButtonText: "Take Quiz"
                    }).then( () => this.startQuiz(), () => { } );
                }
            });
        }
    }

    startQuiz() { 
        let urlRoute = '/classroom/' + this.categoryId + '/test';
        this.router.navigate([urlRoute, this.courseId, this.currentVideoId]);
    }

    playVideo() {
        let video = <HTMLVideoElement>document.getElementById("app-video");
        var isPlaying = video.readyState > 2 && !video.paused && !video.ended;
        if (!isPlaying)     video.play();
    }

    pauseVideo() {
        let video = <HTMLVideoElement>document.getElementById("app-video");
        var isPlaying = video.readyState > 2 && !video.paused && !video.ended;
        if(isPlaying)   video.pause();
    }

    ngOnDestroy() {
        this.pauseVideo();
    }

    toggleTheme() {
        this.toggleMode = (this.toggleMode === "Lights off") ? "Lights on" : "Lights off";
        $(document).ready(function(){
            $("#themetoggle").toggleClass("video-full-size");
        });
    }

    fullscreen() {
        $(document).ready(function(){
            $("#vg-scrub-bar").toggleClass("vg-scrub-bar");
            $("#vg-controls").toggleClass("vg-controls");
            $("#app-video").toggleClass("app-video");
        });
    }
}