import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { HttpInterceptor } from '../../shared/httpInterceptor/httpInterceptor';
import { VideoDescription } from '../../shared/models/videoDesc.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class VideoComponentService {

    test: string;
    constructor(private http: HttpInterceptor) { }

    sendvideotimeservice(reqObj: object) {
        return this.http.put('/api/addDuration', reqObj)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getvideoList(courseId: string): Observable<VideoDescription[]> {
        return this.http.get(`/api/getVideoByCourseId/${courseId}`)
            .map(res => JSON.parse(res._body).result.video as VideoDescription[])
            .catch(this.handleError);
    }

    getDurationOfVideo(userid:string, videoid:string):Promise<any>{
        return this.http.get(`/api/videoDuration/${userid}/${videoid}`)
            .toPromise()
            .then(res => JSON.parse(res._body).data.duration as any)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}
