var mongoose = require('mongoose');
var async = require("async");
var fs = require("fs");
var dataPath = "./data.json"
var content = fs.readFileSync(dataPath);
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/atmecsLearning');
var category = require( '../server/model/category');
var course = require( '../server/model/course');
var conn = mongoose.connection;
var Categories = mongoose.model('category');
var Courses = mongoose.model('course');
var categoriesArray = [];
var coursesArray = [];
var milliseconds = (new Date).getTime();
var categoryContent = JSON.parse(content);

conn.on('error', console.error);
conn.once('open', function() {
	console.log("DB connected..");
	mongoose.connection.db.listCollections({name: 'categories'})
    .next(function(err, collinfo) {
        if (collinfo) {
            console.log("Collection Exist");
        } else{
			console.log("Collection Not Exist");
			for(var i = 0; i < categoryContent.length; i++){
					categoriesArray[i] = new Categories({
						name: categoryContent[i].name,
						description : categoryContent[i].description,
						imgPath : categoryContent[i].imgPath,
						quotes : categoryContent[i].quotes,
						like : [],
						status : categoryContent[i].status					
					});
			}
			for(category in categoriesArray){
				categoriesArray[category].save(function(err, result){
					if(err){
					  console.log("Category Not Save");
					}else{
						for(var i = 0; i < categoryContent.length; i++){
							if(result.name == categoryContent[i].name){
								for(var j = 0; j < categoryContent[i].course.length; j++){
									coursesArray[j] = new Courses({
										name: categoryContent[i].course[j].name,
										description : categoryContent[i].course[j].description,
										categoryId : result._id,
										// categorys: result._id,
										status : categoryContent[i].course[j].status
									});
									coursesArray[j].video.push({ name: categoryContent[i].course[j].video.name, desc: 
											categoryContent[i].course[j].video.desc, path: categoryContent[i].course[j].video.path})
								}
							}
						}
						for(course in coursesArray){
							coursesArray[course].save(function(err, result){
								if(err){
									console.log("Course Not Save");
								}else{								
									console.log("Course Save: "+coursesArray[course].name);
								}
							});
						}
						// categoriesArray[category].courses.push(coursesArray);
						// categoriesArray[category].save();
						console.log("Category Save: "+categoriesArray[category].name);
					}
				});
			}
		}
    }); 
});