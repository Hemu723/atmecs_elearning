var consts = require( './const');
var mongoose = require('mongoose');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

var login = require( '../server/model/register');
var profile = require( '../server/model/profile');

var Register = mongoose.model('login');
var Profile = mongoose.model('profile');

module.exports = function (passport) {

// used to serialize the user for the session
passport.serializeUser(function(user, done) {
    done(null, user.id);
});

// used to deserialize the user
passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});

passport.use(new GoogleStrategy({
    clientID: consts.clientId,
    clientSecret: consts.clientSecret,
    callbackURL: "http://localhost:3000/auth/google/callback"
  },
  function(accessToken, refreshToken, profile, done) {
        // var regex = /^[a-z0-9](\.?[a-z0-9]){5,}@atmecs\.com$/
        var emailStatus = profile.emails[0].value.match(consts.atmecs_mail_regex);
        console.log("----------------------------");
        console.log(emailStatus);
        console.log("----------------------------");
        if(emailStatus == null){
            return done(null);
        } else {
            Register.findOne({ googleId : profile.id }, function(err, user) {
                console.log(user)
                if (err)
                    return done(err);
                if (user) {
                    // if a user is found, log them in
                    return done(null, user);
                } else {
                    // if the user isnt in our database, create a new user
                    var newUser = new Register({
                        name: profile.displayName,
                        mobile: null,
                        email: profile.emails[0].value,
                        username: profile.emails[0].value,
                        password: profile.emails[0].value,
                        status: true,
                        token: accessToken,
                        role: "USER",
                        googleId: profile.id,
                        image: profile._json.image.url
                    })
                    console.log("In new User", newUser)
                    // save the user
                    newUser.save(function(err, result) {
                        if (err) {
                                throw err;
                                // res.send({status: false, message: consts.fail_message, err});
                        } else {
                            var userProfile = new Profile({
                                name : profile.displayName,
                                mobile : null,
                                email : profile.emails[0].value,
                                userId : result._id,
                                gender : "",
                                dob : new Date("2013-10-01T00:00:00.000Z"),
                                courses: [],
                                quiz: [],
                                image: profile._json.image.url
                            });
                            console.log("In profile", userProfile)
                            userProfile.save(function (err) {
                                if (err){
                                    console.log(err)
                                }
                            });
                            newUser.userProfile = userProfile;
                            // register.userProfile.push(profile);
                            newUser.save();
                            //res.send({status: true, message: consts.success_message });
                            return done(null, newUser);
                        }
                    });
                }
            })
    }
  }
));
}