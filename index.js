var db = require( './config/db');
var consts = require( './config/const');
var express = require('express');
var passport = require('passport');
async = require('async');
var cookie = require('cookie-parser');

var mongoose = require('mongoose');
var bodyParser = require("body-parser");
var app = express();
var portNumber = 3000;
 
require( './config/passport')(passport)
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb'}));
app.use(bodyParser.json({limit: '50mb'}));
app.use(passport.initialize());
app.use(passport.session());
app.use(cookie());

var category = require('./server/route/category');
var course = require('./server/route/course');
var register = require('./server/route/register');
var profile = require('./server/route/profile');
var quiz = require('./server/route/quiz');

app.get('/auth/google',
  passport.authenticate('google', { scope: ['profile', 'email']
  })
)

app.get('/auth/google/callback', 
  passport.authenticate('google', { failureRedirect: '/login' }),
  function(req, res) {
    //console.log(req.user.userProfile);
    console.log("success");
    console.log(req);
    res.redirect('/googleauth/' + req.user._id);
});  
app.get('/api/getUserProfile/:id', register.getUserProfile);
app.get('/api/getAllCategories/:userId', category.getAllCategories);
app.get('/api/getDetailedCategories', category.getDetailedCategories);

// app.post('/api/likeCall/:categoryId/:likeStatus', category.likeCall);
app.get('/api/likeCall/:categoryId/:userId', category.likeCall);
app.post('/api/addCategory', category.addCategory);
app.put('/api/updateCategory', category.updateCategory);
app.delete('/api/deleteCategory/:categoryId', category.deleteCategory);
app.post('/api/addComment', category.addComment);
app.post('/api/deleteComment', category.deleteComment);
app.get('/api/getCategoryId/:categoryId', category.getCategoryId);
app.post('/api/uploadImageFile', category.uploadImageFile);
app.put('/api/deleteImage', category.deleteImage);

app.get('/api/courses/:_id', course.courseById);
app.get('/api/category/:categoryId', course.categoryById);
app.get('/api/getClasroomVideo/:userId/:courseId/:videoId', course.getClasroomVideo);
app.post('/api/addCourse', course.addCourse);
app.put('/api/updateCourse', course.updateCourse);
app.put('/api/updateVideo', course.updateVideo);
app.delete('/api/deleteCourse/:courseId', course.deleteCourse);
app.post('/api/uploadVideoFile/:categoryId/:courseId/:videoName/:videoDesc', course.uploadVideoFile);
app.get('/api/getVideoByCourseId/:courseId', course.getVideoByCourseId);
app.get('/api/getVideoById/:videoId/:courseId', course.getVideoById);
app.put('/api/deleteVideoById', course.deleteVideoById);

app.put('/api/updateVideoPriority', course.updateVideoPriority);
app.post('/api/addQuestion', quiz.addQuestion);
app.put('/api/updateQuestion', quiz.updateQuestion);
app.delete('/api/deleteQuestion/:quizId', quiz.deleteQuestion);
app.get('/api/getRandomQuestion/:videoId', quiz.getRandomQuestion);
app.get('/api/getAllQuestions/:courseId', quiz.getAllQuestions);
app.get('/api/getQuestionsByVideoId/:videoId', quiz.getQuestionsByVideoId);

app.post('/api/submitQuiz', quiz.submitQuiz);
app.get('/api/getQuestionById/:questionId', quiz.getQuestionById);

app.post('/api/register', register.registerUser);
app.post('/api/login', register.loginUser);
app.put('/api/changePassword', register.changePassword);
app.put('/api/getSecurityQues', register.getSecurityQues);
app.put('/api/forgetPassword', register.forgetPassword);
app.put('/api/updateRole/:userId/:role', register.updateRole);
app.put('/api/updateStatus/:userId/:status', register.updateStatus);
app.get('/api/getAllUser', register.getAllUser);
app.get('/api/verifyEmail/:email', register.verifyEmail);
app.get('/api/verifyUsername/:username', register.verifyUsername);


app.get('/api/getQuizResult/:userId', profile.getQuizResult);
app.put('/api/updateUserProfile', profile.updateUserProfile);
app.get('/api/getAllUserQuiz', profile.getAllUserQuiz);
app.get('/api/getUserQuiz/:userId', profile.getUserQuiz);
app.put('/api/addDuration', profile.setVideoDuration);
app.get('/api/videoDuration/:userId/:videoId', profile.getVideoDuration);

app.use('/', express.static(__dirname + '/'));
app.use('/app', express.static(__dirname + '/app'));
app.use('/node_modules', express.static(__dirname + '/node_modules'));

app.get('*', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.listen(portNumber, function() {
  console.log("server started on port: "+portNumber);
});