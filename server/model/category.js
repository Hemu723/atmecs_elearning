var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;
var course = require( './course');

var CategorySchema = new Schema({
    name : String,
    description : String,
    imgPath : String,
    quotes : String,
    like : [],
    comments : [{
        userId: String,
        message: String
    }],
    status : String,
    courses : [{ type: Schema.ObjectId, ref: 'course' }]
});
mongoose.model('category', CategorySchema);