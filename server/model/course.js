var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;
var category = require( './category');

var CourseSchema = new Schema({
    name : String,
    description : String,
    video : [{
        id: String, 
        name: String,
        creationTime: {type: Date, default: Date.now},
        priority: Number,
        desc: String, 
        path: String, 
        courseId: String,
        status : String,
        like: Number,
        comments : [{
            userId: String,
            message: String
        }],
        questions: []
    }],
    categoryId : String,
    status : String,
    categorys : { type: Schema.ObjectId, ref: 'category' }
});
mongoose.model('course', CourseSchema);