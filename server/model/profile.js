var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var ProfileSchema = new Schema({
    name : String,
    mobile : Number,
    email : String,
    userId : String,
    dob : Date,
    gender: String,
    courses: [],
    doj: {type: Date, default: Date.now},
    quiz: [{categoryId: String, courseId: String, categoryName: String, courseName: String, marksObtain: Number, totalMarks: Number, totalPoint: Number, dot: {type: Date, default: Date.now}}],
    video: [{categoryId: String, courseId: String, videoId: String, duration: String, modificationTime: Date, creationTime: {type: Date, default: Date.now} }],
    image: String
});
mongoose.model('profile', ProfileSchema);