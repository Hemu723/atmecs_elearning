var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;

var QuizSchema = new Schema({
    question : String,
    options : {
        opt1: String,
        opt2: String,
        opt3: String,
        opt4: String,
        opt5: String
    },
    optionField: String,
    answer: String,
    point : Number,
    videoId : String,
    courseId: String,
    note: String,
    status: Boolean,
    random: Number
});
mongoose.model('quiz', QuizSchema);
// QuizSchema.index({random: 1}, {unique: true});