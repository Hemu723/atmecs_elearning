var mongoose = require( 'mongoose' );
var Schema   = mongoose.Schema;
var story = require( './profile');

var RegisterSchema = new Schema({
    name : String,
    mobile : Number,
    email : String,
    username : String,
    password : String,
    status : Boolean,
    token : String,
    role : String,
    googleId: String,
    image: String,
    question1 : {q1: String, a1: String},
    question2 : {q2: String, a2: String},
    
    //createion time
   //modification time
   //login time

    userProfile : { type: Schema.ObjectId, ref: 'profile' }
});
mongoose.model('login', RegisterSchema);