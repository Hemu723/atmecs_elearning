var express = require('express');
var category = require('../model/category');
var course = require('../model/course');
var mongoose = require('mongoose');
var multer = require('multer');
var mkdirp = require('mkdirp');
var bodyParser = require('body-parser');
var consts = require('../../config/const');
var fs = require('fs');

var Courses = mongoose.model('course');
var Categories = mongoose.model('category');

var limits = {
    fieldNameSize: 10000,
    files: 10,
    fields: 15
}

var imageName;
var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        console.log(req.filePath);
        callback(null, './' + req.filePath);
    },
    filename: function (req, file, callback) {
        if (imageName != undefined)
            callback(null, imageName);
        else {
            file.fileName = 'image' + '-' + Date.now() + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]
            callback(null, file.fileName);
        }
    }
});

var uploadImage = multer({ storage: storage, limits: limits }).single('file');

//Get all categories
exports.getAllCategories = function (req, res) {
    if(req.headers.userrole == "ADMIN" && req.headers.flag==1) {
        query = {};
    } else {
        query = {status: "Active"};
    }
    Categories.find(query , function (err, result) {
        if (err) {
            return console.error(err);
        } else {
            for (var i = 0; i < result.length; i++) {
                likeStatus = false;
                for (var j = 0; j < result[i].like.length; j++) {
                    if (result[i].like[j] == req.params.userId) {
                        likeStatus = true;
                    }
                }
                result[i] = result[i].toObject();
                if (likeStatus == true) {
                    result[i].likeFlag = true;
                } else {
                    result[i].likeFlag = false;
                }
                result[i].likeCount = result[i].like.length;
                delete result[i].like;
            }
            res.send(result);
        }
    });
}

//Like operations
exports.likeCall = function (req, res) {
    // var like;
    // if(req.params.likeStatus == 0) {
    //     like = -1;
    // } else if(req.params.likeStatus == 1) {
    //     like = 1;
    // }    
    // Categories.update({"_id": id}, { $inc : { "like" : like } }, function(err, result) {
    //     if (err) return console.error(err);
    //     res.send({status: true, result});
    // });

    Categories.count({ "_id": req.params.categoryId, "like": { $in: [req.params.userId] } }, function (err, count) {
        if (err) return console.error(err);
        else {
            console.log(count);
            if (count == 0) {
                Categories.update({ "_id": req.params.categoryId }, { $push: { "like": req.params.userId } }, function (err, result) {
                    if (err) return console.error(err);
                    res.send({ status: true, message: consts.added });
                });
            } else if (count == 1) {
                Categories.update({ "_id": req.params.categoryId }, { $pull: { "like": req.params.userId } }, function (err, result) {
                    if (err) return console.error(err);
                    res.send({ status: true, message: consts.deleted });
                });
            }
        }
    });
}

//Add Category
exports.addCategory = function (req, res) {
    var category = new Categories({
        name: req.body.name,
        description: req.body.description,
        imgPath: req.body.imgPath,
        quotes: req.body.quotes,
        like: [],
        status: "Active"
    })
    category.save(function (err, result) {
        if (err) {
            console.log("Category Saved");
            res.send({ status: false, message: consts.fail, err });
        } else {
            res.send({ status: true, message: consts.success, result });
        }
    });
}

//Update Category and update image
exports.updateCategory = function (req, res) {
    //important don't delete it
    // Categories.update({"_id": req.body.id},  { $set: { name: req.body.name, description: req.body.description,
    //      imgPath: req.body.imgPath, quotes: req.body.quotes }}, { multi: true }, function(err, result) {
    //     if (err){
    //         res.send({status: false, message: consts.fail, err});
    //         return console.error(err);
    //     } else {
    //         res.send({status: true, message: consts.success, result});
    //     }        
    // });
    Categories.findById(req.body._id, function (err, categorie) {
        if (err) {
            res.status(500).send({ status: false, err });
        } else {
            categorie.name = req.body.name || categorie.name;
            categorie.description = req.body.description || categorie.description;
            categorie.imgPath = req.body.imgPath || categorie.imgPath;
            categorie.quotes = req.body.quotes || categorie.quotes;
            categorie.status = req.body.status || categorie.status;
            categorie.save(function (err, result) {
                if (err) {
                    res.status(500).send({ status: false, message: consts.fail, err });
                }
                res.send({ status: true, message: consts.success, result });
            });
        }
    });
}

//delete category
exports.deleteCategory = function (req, res) {
    Categories.findByIdAndRemove(req.params.categoryId, function (err, categoryResult) {
        if (err) {
            res.send({ status: false, message: consts.fail, err });
            return console.error(err);
        } else {
            console.log(categoryResult)
            Courses.remove({ "categoryId": req.params.categoryId }, function (err, result) {
                if (err) return console.error(err);
                res.send({ status: true, mesage: consts.success, result })
            });

        }
    });
}

//add comments
exports.addComment = function (req, res) {
    Categories.update({ "_id": req.body.categoryId }, {
        $push: {
            comments: {
                "userId": req.body.userId, "message":
                req.body.message
            }
        }
    }, function (err, result) {
        if (err) return console.error(err);
        res.send({ status: true, message: consts.added, result });
    });
}

//delete comments
exports.deleteComment = function (req, res) {
    Categories.update({ "_id": req.body.categoryId, comments: { $elemMatch: { _id: req.body.commentId } } }, {
        $pull:
        { comments: { "userId": req.body.userId, "_id": req.body.commentId } }
    }, function (err, result) {
        if (err) return console.error(err);
        res.send({ status: true, message: consts.added, result });
    });
}

//get category by id
exports.getCategoryId = function (req, res) {
    Categories.findById(req.params.categoryId, function (err, result) {
        if (err) {
            res.send({ status: false, message: "error occurred", err });
        }
        else {
            res.send({ status: true, message: "display category", result });
        }
    });
}

//Add directory
function addDirectory(path) {
    mkdirp(path, function (err) {
        if (err) {
            console.error(err);
            return false;
        } else {
            return true;
        }
    });
}

//Check directory exist or not
function directoryExist(path) {
    if (fs.existsSync(path)) {
        return true;
    } else {
        return false;
    }
}

//Image upload APIs
exports.uploadImageFile = function (req, res) {
    var imageUrl = consts.imagePath;
    if (directoryExist(consts.assetsPath) && directoryExist(consts.imagePath)) {
        req.filePath = imageUrl;
        uploadImage(req, res, function (err) {
            if (err) {
                res.send({ status: consts.fail, imagePath: undefined })
            } else {
                console.log(req.file)
                res.send({ status: consts.success, imagePath: imageUrl + "/" + req.file.fileName })
            }
        });
    } else {
        mkdirp(imageUrl, function (err) {
            if (err) {
                console.error(err);
                res.send({ status: consts.fail, imagePath: undefined })
            } else {
                req.filePath = imageUrl;
                uploadImage(req, res, function (err) {
                    if (err) {
                        res.send({ status: consts.fail, imagePath: undefined })
                    } else {
                        console.log(req.file)
                        res.send({ status: consts.success, imagePath: imageUrl + "/" + req.file.fileName })
                    }
                });
            }
        });
    }
}

exports.deleteImage = function (req, res) {
    if (directoryExist(req.body.imageURL)) {
        fs.unlink(req.body.imageURL, function (err, result) {
            if (err) return console.log(err);
            else {
                // Categories.findByIdAndRemove(req.body.categoryId, function(err, result){
                //     if (err) {
                //         res.send({ status: false, message: consts.remove_failed });
                //         return console.error(err);
                //     } else {
                //         res.send({ status: true, message: consts.remove_success });
                //     }
                // });
                res.send({ status: true, message: consts.remove_success });
            }
        });
    } else {
        res.send({ status: false, message: consts.fail });
    }
}

exports.getDetailedCategories = function(req, res){
    if(req.headers.userrole="ADMIN" && req.headers.flag==1){
        query={}
     }else{
         query={status:"Active"}
     }
    var viewCategory = {
        name: true,
        courses: true
    }
     var viewCourse = {
         name : true,
         "video._id":true,
         "video.name": true
     }
Categories.find(query, viewCategory).populate('courses', viewCourse).exec(function(err, result){
        if(err){
            console.log(err);
        }else{
            res.send({status:true, message:consts.success, result});
        }
     });
}