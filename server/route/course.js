var express = require('express');
var course = require('../model/course');
var category = require('../model/category');
var fs = require("fs");
var mongoose = require('mongoose');
var consts = require('../../config/const');
var bodyParser = require('body-parser');
var multer = require('multer');
var mkdirp = require('mkdirp');

var Courses = mongoose.model('course');
var Categories = mongoose.model('category');
var Profile = mongoose.model('profile');

var limits = {
    fieldNameSize: 1000000,
    files: 10,
    fields: 15
}

var imageName;
var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        console.log(req.filePath);
        callback(null, './' + req.filePath);
    },
    filename: function (req, file, callback) {
        if (imageName != undefined)
            callback(null, imageName);
        else {
            file.fileName = 'file' + '-' + Date.now() + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]
            callback(null, file.fileName);
        }
    }
});

var uploadVideo = multer({ storage: storage, limits: limits }).single('file');

//list detailed courses by categoryId
exports.categoryById = function (req, res) {
    var query;
    if (req.headers.userrole == "ADMIN" && req.headers.flag == 1)
        query = { "categoryId": req.params.categoryId };
    else
        query = { "categoryId": req.params.categoryId, status: "Active", "video.status": "Active" };
    Courses.aggregate([
        {
            "$unwind":
            {
                "path": "$video",
                "preserveNullAndEmptyArrays": true
            }
        },
        { $sort: { 'video.priority': 1 } },
        { $match: query },
        {
            $group: {
                _id: "$_id",
                name: { $first: "$name" },
                description: { $first: "$description" },
                categoryId: { $first: "$categoryId" },
                status: { $first: "$status" },
                video: { $push: "$video" }
            }
        },
        { $project: { "name": 1, "video": 1, "description": 1, "categoryId": 1, "status": 1 } }
    ], function (err, result) {
        if (err)
            res.status(500).send({ status: false, message: consts.fail, err });
        else
            res.send({ status: true, message: consts.success, result });
    })
}

exports.courseById = function (req, res) {
    Courses.findById(req.params._id, function (err, result) {
        if (err) return console.error(err);
        res.send(result);
    });
}
exports.getClasroomVideo = function (req, res) {
        var videoTime = "000.00";
        Profile.findOne({ userId: req.params.userId, "video": { $elemMatch: { "videoId": req.params.videoId } } }, function (err, result) {
            if (err) {
                console.log(err);
            } else {
                if (result != null)
                    videoTime = result.video[0].duration;
            }
        });
        Courses.find({ "_id": req.params.courseId, video: { $elemMatch: { "_id": req.params.videoId } } }, { "video.$": 1 }, function (err, result) {
            // Courses.findOne({"_id": id}).populate('video').exec(function (err, result) {
            if (err) {
                res.status(500).send({ status: false, message: consts.fail, err });
                return console.error(err);
            } else {
                const movieFile = `./${result[0].video[0].path}`;
                console.log(movieFile);
                try {
                    var stat = fs.statSync(movieFile);
                    var total = stat.size;

                    if (req.headers.range) {
                        var range = req.headers.range;
                        var parts = range.replace(/bytes=/, "").split("-");
                        var partialstart = parts[0];
                        var partialend = parts[1];

                        var start = parseInt(partialstart, 10);
                        var end = partialend ? parseInt(partialend, 10) : total - 1;
                        var chunksize = (end - start) + 1;
                        console.log('RANGE: ' + start + ' - ' + end + ' = ' + chunksize);

                        var file = fs.createReadStream(movieFile, { start: start, end: end });
                        res.writeHead(206, { 'Content-Range': 'bytes ' + start + '-' + end + '/' + total, 'Accept-Ranges': 'bytes', 'Content-Length': chunksize, 'Content-Type': 'video/mp4', 'Content-duration': videoTime });
                        file.pipe(res);

                    } else {
                        console.log('ALL: ' + total);
                        res.writeHead(200, { 'Content-Length': total, 'Content-Type': 'video/mp4', 'Content-duration': videoTime });
                        fs.createReadStream(movieFile).pipe(res);
                    }
                } catch (err) {
                    console.log('Error:', err);
                    res.status(500).send({ status: false, message: consts.video_not_available, err });
                }
            }
        });
    }

    exports.addCourse = function (req, res) {
        var course = new Courses({
            name: req.body.name,
            description: req.body.description,
            categoryId: req.body.categoryId,
            categorys: req.body.categoryId,
            status: "Active"
        });
        course.save(function (err, result) {
            if (err) {
                res.status(500).send({ status: false, message: consts.fail, err });
            } else {
                Categories.findOneAndUpdate({ "_id": req.body.categoryId }, { $push: { courses: course } }, { new: true, upsert: true },
                    function (err, category) {
                        if (err) return console.error(err);
                        res.send({ status: true, message: consts.success, result });
                    });
            }
        });
    }
    exports.updateCourse = function (req, res) {
        Courses.findByIdAndUpdate(req.body._id, {
            $set: {
                name: req.body.name, description: req.body.description,
                status: req.body.status
            }
        }, { multi: true }, function (err, result) {
            console.log(result);
            if (err) {
                res.send({ status: false, message: consts.fail, err });
                return console.error(err);
            } else {
                res.send({ status: true, message: consts.success, result });
            }
        });
    }

    exports.updateVideo = function (req, res) {
        Courses.update({ "_id": req.body.courseId, video: { $elemMatch: { _id: req.body.videoId } } }, {
            $set: {
                "video.$.name": req.body.video.name, "video.$.desc": req.body.video.description,
                "video.$.path": req.body.video.path, "video.$.status": req.body.video.status
            }
        }, { multi: true }, function (err, result) {
            console.log(result)
            if (err) {
                res.send({ status: false, message: consts.fail, err });
                return console.error(err);
            } else {
                res.send({ status: true, message: consts.success, result });
            }
        });
    }

    exports.deleteCourse = function (req, res) {
        Courses.findByIdAndRemove(req.params.courseId, function (err, result) {
            if (err) {
                res.send({ status: false, message: consts.fail, err });
                return console.error(err);
            } else {
                Categories.findOneAndUpdate({ "_id": result.categoryId }, { $pull: { courses: req.params.courseId } },
                    { new: true, upsert: true }, function (err, category) {
                        if (err) return console.error(err);
                        res.send({ status: true, message: consts.success, result });
                    });
                for (var i = 0; i < result.video.length; i++) {
                    if (directoryExist(result.video[i].path)) {
                        fs.unlink(result.video[i].path, function (err, result) {
                            if (err) console.log(err)
                            else {
                                console.log("file deleted successfully");
                            }
                        });
                    } else {
                        res.send({ status: false, message: consts.fail });
                    }
                }
            }
        });
    }

    function addDirectory(path) {
        mkdirp(path, function (err) {
            if (err) {
                console.error(err);
                return false;
            } else {
                return true;
            }
        });
    }

    function directoryExist(path) {
        if (fs.existsSync(path)) {
            return true;
        } else {
            return false;
        }
    }

    function addVideoInDB(courseId, videoName, videoDesc, videoPath, res) {
        Courses.aggregate(
            [
                { $match: { "_id": mongoose.Types.ObjectId(courseId) } }
                , {
                    $project: {
                        _id: 0,
                        videoSize: { $size: "$video" }
                    }
                }
            ], function (err, count) {
                if (count != undefined) {
                }
                if (err) {
                    return console.error(err);
                } else {
                    Courses.findByIdAndUpdate(courseId, {
                        $push: {
                            video: {
                                "name": videoName, "priority": count[0].videoSize + 1,
                                "desc": videoDesc, "path": videoPath, status: "Active"
                            }
                        }
                    },
                        function (err, result) {
                            if (err) {
                                res.send({ status: false, message: consts.fail, err });
                                return console.error(err);
                            } else {
                                res.send({ status: true, message: consts.success, result });
                            }
                        });
                }
            });
    }
    exports.updateVideoPriority = function (req, res) {
        Courses.aggregate([
            { "$unwind": "$video" },
            { $match: { _id: mongoose.Types.ObjectId(req.body.courseId), "video.priority": req.body.newPriority } }
            , {
                $project: {
                    _id: 0,
                    "video._id": 1
                }
            },
        ]
            , function (newVideoIdErr, newVideoId) {
                if (newVideoIdErr) console.log(newVideoIdErr);
                else {
                    if (newVideoId.length != 0) {
                        Courses.update({ _id: req.body.courseId, video: { $elemMatch: { "_id": req.body.videoId } } }, {
                            $set: { "video.$.priority": req.body.newPriority }
                        },
                            function (swapOneErr, swapOneResult) {
                                if (swapOneErr) return handleError(swapOneErr);
                                else {
                                    Courses.update({ _id: req.body.courseId, video: { $elemMatch: { "_id": newVideoId[0].video._id } } }, {
                                        $set: { "video.$.priority": req.body.oldPriority }
                                    },
                                        function (swapTwoErr, swapTwoResult) {
                                            if (swapTwoErr) return handleError(swapTwoErr);
                                            else {
                                                res.send({ status: true, message: consts.success, swapTwoResult })
                                            }
                                        });
                                }
                            });
                    } else {
                        Courses.update({ _id: req.body.courseId, video: { $elemMatch: { "_id": req.body.videoId } } }, {
                            $set: { "video.$.priority": req.body.newPriority }
                        }, function (err, result) {
                            if (err) console.log(err);
                            else {
                                res.send({ status: true, message: consts.success, result })
                            }
                        });
                    }
                }
            });
    }

    //alternative only file upload APIs
    exports.uploadVideoFile = function (req, res) {
        var category = req.params.categoryId;
        var course = req.params.courseId;
        var videoName = req.params.videoName;
        var videoDesc = req.params.videoDesc;
        var videoUrl = consts.videoPath + "/" + category + "/" + course;
        if (directoryExist(consts.assetsPath) && directoryExist(consts.videoPath) && directoryExist(consts.videoPath + "/"
            + category) && directoryExist(videoUrl)) {
            req.filePath = videoUrl;
            uploadVideo(req, res, function (err) {
                if (err) {
                    res.send({ status: consts.fail, videoPath: undefined })
                } else {
                    console.log(req.file)
                    addVideoInDB(course, videoName, videoDesc, videoUrl + "/" + req.file.fileName, res);
                    // res.send({status: consts.success, videoPath: videoUrl + "/" + req.file.fileName})
                }
            });
        } else {
            mkdirp(videoUrl, function (err) {
                if (err) {
                    console.error(err);
                    res.send({ status: consts.fail, videoPath: undefined })
                } else {
                    req.filePath = videoUrl;
                    uploadVideo(req, res, function (err) {
                        if (err) {
                            res.send({ status: consts.fail, videoPath: undefined })
                        } else {
                            console.log(req.file)
                            addVideoInDB(course, videoName, videoDesc, videoUrl + "/" + req.file.fileName, res);
                            // res.send({status: consts.success, videoPath: videoUrl + "/" + req.file.fileName})
                        }
                    });
                }
            });
        }
    }

    // get Video Name & video Id by course ID
    exports.getVideoByCourseId = function (req, res) {
        if (req.headers.userrole == "ADMIN" && req.headers.flag == 1) {
            var query = { _id: mongoose.Types.ObjectId(req.params.courseId) }
        } else {
            var query = { _id: mongoose.Types.ObjectId(req.params.courseId), status: "Active", "video.status": "Active" }
        }

        Courses.aggregate([
            {
                "$unwind":
                {
                    "path": "$video",
                    "preserveNullAndEmptyArrays": true
                }
            },
            { $match: query },
            { $sort: { 'video.priority': 1 } },
            {
                $group: {
                    _id: "$_id",
                    video: { $push: "$video" }
                }
            },
            { $project: { "_id": 0, "video": 1 } }
        ], function (err, result) {
            if (err)
                res.status(500).send({ status: false, message: consts.fail, err });
            else {
                result = result[0];
                res.send({ status: true, message: consts.success, result });
            }
        })

        // Courses.findById(req.params._id, function (err, result) {
        //     if (err) return(err);
        //     else {
        //         // result = result.toObject();
        //         // delete result._id
        //         res.send(result);
        //     }
        //     }).select({"video.name":1, "video._id":1, "_id":0});
        //     result.video = result.toObject().video.sort(function (m1, m2) { return m1.priority - m2.priority; });
    }
    exports.getVideoById = function (req, res) {
        var viewProjection = {
            "_id": true,
            "video.name": true,
            "video.desc": true,
            "video.path": true,
            "video._id": true,
            "video.status": true
        }
        Courses.findOne({ "_id": req.params.courseId }, { "video": { $elemMatch: { "_id": req.params.videoId } } }, function (err, result) {
            if (err) return (err);
            else {
                var data = {};
                data.name = result.video[0].name;
                data.desc = result.video[0].desc;
                data.path = result.video[0].path;
                data.status = result.video[0].status;
                res.send({ status: true, message: consts.success, data });
            }
        });
    }

    exports.deleteVideoById = function (req, res) {
        if (directoryExist(req.body.videoURL)) {
            fs.unlink(req.body.videoURL, function (err, result) {
                if (err) return console.log(err);
                else {
                    Courses.findOneAndUpdate({ "video": { $elemMatch: { "_id": req.body.videoId } } }, { $pull: { video: { _id: req.body.videoId } } }, function (err, result) {
                        if (err) {
                            res.send({ status: false, message: consts.remove_failed });
                            return console.error(err);
                        } else {
                            res.send({ status: true, message: consts.remove_success });
                        }
                    });
                }
                console.log('file deleted successfully');
            });
        } else {
            res.send({ status: false, message: consts.fail });
        }
    }