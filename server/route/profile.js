var express = require('express');
var profile = require('../model/profile');
var login = require('../model/register');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var consts = require('../../config/const');

var Profile = mongoose.model('profile');
var Register = mongoose.model('login');


exports.getQuizResult = function (req, res) {
    var viewQuiz = {
        "_id": false,
        "quiz.categoryName": true,
        "quiz.courseName": true,
        "quiz.categoryId": true,
        "quiz.courseId": true,
        "quiz.marksObtain": true,
        "quiz.totalMarks": true,
        "quiz.dot": true
    }

    Profile.findOne({ "userId": req.params.userId }, viewQuiz, function (err, result) {
        if (err) {
            res.status(500).send({ status: false, message: consts.fail, err });
            return console.error(err);
        }
        res.send({ status: true, message: consts.success, result });
    });
}

//Edit user Profile
exports.updateUserProfile = function (req, res) {
    Profile.update({ "userId": req.body.userId }, { $set: { mobile: req.body.mobile, email: req.body.email } }, { multi: true }, function (err, result) {
        if (err) {
            res.send({ status: false, message: consts.fail, err });
            return console.error(err);
        } else {
            res.send({ status: true, message: consts.success, result });
        }
    });
}

//to get all the Quiz results
exports.getAllUserQuiz = function (req, res) {
    Profile.aggregate([
        { $unwind: "$quiz" },
        {
            $group:
            {
                _id: "$_id",
                userId: { $first: "$userId" },
                name: { $first: "$name" },
                quiz: { $push: "$quiz" },
                avgMarksObtain: { $avg: '$quiz.marksObtain' },
                avgTotalMarks: { $avg: '$quiz.totalMarks' },
                sumMarksObtain: { $sum: '$quiz.marksObtain' },
                sumTotalMarks: { $sum: '$quiz.totalMarks' },
                count: { $sum: 1 }
            }
        },
        {
            $project:
            {
                "_id": 0, "userId": 1, "quiz": 1, "name": 1, "calculations":
                {
                    "avgMarksObtain": "$avgMarksObtain",
                    "avgTotalMarks": "$avgTotalMarks",
                    "sumMarksObtain": "$sumMarksObtain",
                    "sumTotalMarks": "$sumTotalMarks",
                    "percentage": { $multiply: ["$sumMarksObtain", { $divide: [100, "$sumTotalMarks"] }] },
                    "count": "$count"
                }
            }
        }
    ], function (err, result) {
        res.send({ status: true, message: consts.success, result });
    });
}

//get Quiz result of a particular user
exports.getUserQuiz = function (req, res) {
    Profile.aggregate([
        { $match: { userId: req.params.userId } },
        { $unwind: "$quiz" },
        {
            $group:
            {
                _id: "$_id",
                userId: { $first: "$userId" },
                name: { $first: "$name" },
                quiz: { $push: "$quiz" },
                avgMarksObtain: { $avg: '$quiz.marksObtain' },
                avgTotalMarks: { $avg: '$quiz.totalMarks' },
                sumMarksObtain: { $sum: '$quiz.marksObtain' },
                sumTotalMarks: { $sum: '$quiz.totalMarks' },
                count: { $sum: 1 }
            }
        },
        {
            $project:
            {
                "_id": 0, "userId": 1, "quiz": 1, "name": 1, "calculations":
                {
                    "avgMarksObtain": "$avgMarksObtain",
                    "avgTotalMarks": "$avgTotalMarks",
                    "sumMarksObtain": "$sumMarksObtain",
                    "sumTotalMarks": "$sumTotalMarks",
                    "percentage": { $multiply: ["$sumMarksObtain", { $divide: [100, "$sumTotalMarks"] }] },
                    "count": "$count"
                }
            }
        }
    ], function (err, result) {
        result = result[0];
        res.send({ status: true, message: consts.success, result });
    });
}

//storing video duration as per user and video Id
exports.setVideoDuration = function (req, res) {
    Profile.findOneAndUpdate({ userId: req.body.userId, "video": { $elemMatch: { "videoId": req.body.videoId } } }, {
        $set: {
            "video.$.duration": req.body.duration, "video.$.modificationTime": Date.now()
        }
    }, function (err, modificationResult) {
        console.log(modificationResult);
        if (err) {
            res.send({ status: false, message: consts.fail, err });
        } else {
            if (modificationResult == null) {
                Profile.update({ userId: req.body.userId }, {
                    $push: {
                        video: {
                            "categoryId": req.body.categoryId, "courseId": req.body.courseId, "videoId": req.body.videoId,
                            "duration": req.body.duration, "modificationTime": Date.now()
                        }
                    }
                },
                    function (err, additionResult) {
                        if (err) return console.error(err);
                        res.send({ status: true, message: consts.success, value: "Added" });
                    });
            } else
                res.send({ status: true, message: consts.success, value: "Modified" });
        }
    });
}

//get video duration as per user and video Id
exports.getVideoDuration = function (req, res) {
   let viewDetails = {
       _id : 0,
       "video.duration" : 1,
       "video.modificationTime" : 1
   }
   Profile.findOne({ userId: req.params.userId}, { "video": { $elemMatch: { "videoId": {"$in":[req.params.videoId]} } } }, function (err, result) {
       if (err)
           res.send({ status: false, message: consts.fail, err });
       else {
           console.log(result);
           let data = {};
           if(result.video.length != 0){
               data.modificationTime = result.video[0].modificationTime;
               data.duration = result.video[0].duration;
           } else {
               data = {duration: 0.00};
           }
           res.send({ status: true, message: consts.success, data });
       }
   });
}
