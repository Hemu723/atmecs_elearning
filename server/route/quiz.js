var express = require('express');
var async = require("async");
var quiz = require('../model/quiz');
var profile = require('../model/profile');
require('../model/course');
require('../model/category');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var consts = require('../../config/const');

var Quiz = mongoose.model('quiz');
var Profile = mongoose.model('profile');

var Courses = mongoose.model('course');
var Categories = mongoose.model('category');

exports.addQuestion = function (req, res) {
    var quiz = new Quiz({
        question: req.body.question,
        options: {
            opt1: req.body.options.opt1,
            opt2: req.body.options.opt2,
            opt3: req.body.options.opt3,
            opt4: req.body.options.opt4,
            opt5: req.body.options.opt5
        },
        optionField: req.body.optionField,
        answer: req.body.answer,
        point: req.body.point,
        note: req.body.note,
        videoId: req.body.videoId,
        courseId: req.body.courseId,
        status: true,
        random: Math.random()
    });
    quiz.save(function (err, result) {
        if (err) {
            res.status(500).send({ status: false, message: consts.fail, err });
        } else {
            res.send({ status: true, message: consts.success, result });
        }
    });
}

exports.updateQuestion = function (req, res) {
    Quiz.findById(req.body.id, function (err, quiz) {
        console.log(req.body)
        if (err) {
            res.status(500).send({ status: false, err });
        } else {
            quiz.question = req.body.question || quiz.question;
            quiz.options.opt1 = req.body.options.opt1 || quiz.options.opt1;
            quiz.options.opt2 = req.body.options.opt2 || quiz.options.opt2;
            quiz.options.opt3 = req.body.options.opt3 || quiz.options.opt3;
            quiz.options.opt4 = req.body.options.opt4 || quiz.options.opt4;
            quiz.options.opt5 = req.body.options.opt5 || quiz.options.opt5;
            quiz.optionField = req.body.optionField || quiz.optionField;
            quiz.answer = req.body.answer || quiz.answer;
            quiz.point = req.body.point || quiz.point;
            quiz.note = req.body.note || quiz.note;
            quiz.videoId = req.body.videoId || quiz.videoId;
            quiz.courseId = req.body.courseId || quiz.courseId;
            quiz.status = req.body.status || quiz.status;

            quiz.save(function (err, result) {
                if (err) {
                    res.status(500).send({ status: false, message: consts.fail, err });
                }
                res.send({ status: true, message: consts.success, result });
            });
        }
    });
}

exports.deleteQuestion = function (req, res) {
    Quiz.findByIdAndRemove(req.params.quizId, function (err, result) {
        if (err) {
            res.send({ status: false, message: consts.fail, err });
            return console.error(err);
        } else {
            res.send({ status: true, message: consts.success, result });
        }
    });
}

//Get Quiz
exports.getRandomQuestion = function (req, res) {
    // randomNo = Math.random();
    // console.log(randomNo)
    // Quiz.find({courseId: req.params.courseId, random: { $gte: randomNo }}, {}, { limit: 10 }, function(err, result) {
    // if (err){
    //     res.send({status: false, message: consts.fail, err});
    //     return console.error(err);
    // } else {
    //     res.send({status: true, message: consts.success, result});
    // }        
    // });

    //alternative method but it may repeat the questions
    if (req.headers.userrole == "ADMIN") {
        query = { videoId: req.params.videoId };
    } else {
        query = { videoId: req.params.videoId, status: true };
    }
    Quiz.aggregate([{ $match: query }, { $sample: { size: 10 } }], function (err, result) {
        if (err) {
            res.send({ status: false, message: consts.fail, err });
            return console.error(err);
        } else {
            for (quiz in result) {
                if (result.hasOwnProperty(quiz)) {
                    var option = [];
                    for (var i = 0; i < Object.keys(result[quiz].options).length; i++) {
                        option.push(result[quiz].options[Object.keys(result[quiz].options)[i]])
                    }
                    if (req.headers.userrole !== "ADMIN") {
                        delete result[quiz].answer;
                    }
                    delete result[quiz].options;
                    result[quiz].options = option;
                }
            }
            res.send({ status: true, message: consts.success, result });
        }
    });
}

exports.getAllQuestions = function (req, res) {
    Quiz.aggregate([{ $match: { courseId: req.params.courseId } }], function (err, result) {
        if (err) {
            res.send({ status: false, message: consts.fail, err });
            return console.error(err);
        } else {
            for (quiz in result) {
                if (result.hasOwnProperty(quiz)) {
                    var option = [];
                    for (var i = 0; i < Object.keys(result[quiz].options).length; i++) {
                        option.push(result[quiz].options[Object.keys(result[quiz].options)[i]])
                    }
                    delete result[quiz].options;
                    result[quiz].options = option;
                }
            }
            res.send({ status: true, message: consts.success, result });
        }
    });
}
//get questions on the basis of videoId
exports.getQuestionsByVideoId = function(req, res){
    var query;
    if(req.headers.usserole == "ADMIN" && flag==1){
        query = {videoId :req.params.videoId };
    }else {
        query = { videoId: req.params.videoId, status: true}
    }
    Quiz.aggregate([{ $match: query }], function (err, result) {
        if (err) {
            res.send({ status: false, message: consts.fail, err });
            return console.error(err);
        } else {
            for (quiz in result) {
                if (result.hasOwnProperty(quiz)) {
                    var option = [];
                    for (var i = 0; i < Object.keys(result[quiz].options).length; i++) {
                        option.push(result[quiz].options[Object.keys(result[quiz].options)[i]])
                    }
                    if (req.headers.userrole !== "ADMIN") {
                        delete result[quiz].answer;
                    }
                    delete result[quiz].options;
                    result[quiz].options = option;
                }
            }
            res.send({ status: true, message: consts.success, result });
        }
    });
}

//Submit Quiz
exports.submitQuiz = function (req, res) {
    var categoryName;
    var courseName;

    getCategoryName()
    getCourseName();
    var correctAnswer = 0;
    console.log(req.body);
    for (var i = 0; i < req.body.quiz_resp.length; i++) {
        Quiz.count({ "_id": req.body.quiz_resp[i].q_id, "answer": req.body.quiz_resp[i].submit_opt }, function (err, count) {
            if (err) {
                return console.error(err);
            } else
                if (count == 1)
                    correctAnswer += 1;
        });
    }
    function myFunc(arg) {
        Profile.update({ userId: req.body.userId }, {
            $push: {
                quiz: {
                    "categoryId": req.body.categoryId, "courseId": req.body.courseId, "categoryName": categoryName, 
                    "courseName": courseName, "marksObtain": correctAnswer, "totalMarks": req.body.quiz_resp.length
                }
            }
        },
            function (err, result) {
                console.log(result);
                if (err) return console.error(err);
                res.send({ status: true, message: consts.success, totalMarks: req.body.quiz_resp.length, markObtain: correctAnswer });
            });
    }
    function getCategoryName() {
        var name = null;
        console.log(req.body.categoryId)
        Categories.findById(req.body.categoryId, function (err, result) {
            console.log(result)
            if (err) {
                console.log(err);
                return name;
            } else {
                console.log("---------0 "+result.name)
                categoryName = result.name;
            }
        });
    }
    function getCourseName() {
        var name = null;
        Courses.findById(req.body.courseId, function (err, result) {
            if (err) {
                return name;
            } else {
                courseName = result.name;
            }
        });
    }
    setTimeout(myFunc, 500, 'anyAgrument'); //anyAgrument for future use    
}

// Get question by id
exports.getQuestionById = function (req, res) {
    Quiz.findById(req.params.questionId, function (err, result) {
        if (err) return console.error(err);
        res.send({ status: true, message: consts.success, result });
    });
}