var express = require('express');
var login = require('../model/register');
var profile = require('../model/profile');
var consts = require('../../config/const');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var randtoken = require('rand-token');
var nodemailer = require('nodemailer');

var Register = mongoose.model('login');
var Profile = mongoose.model('profile');

exports.registerUser = function (req, res) {
    q1 = "";
    q2 = "";
    a1 = "";
    a2 = "";
    if (req.body.question1 != undefined && req.body.question2 != undefined) {
        q1 = req.body.question1.q1;
        q2 = req.body.question2.q2;
        a1 = req.body.question1.a1;
        a2 = req.body.question2.a2;
    }
    var register = new Register({
        name: req.body.name,
        mobile: req.body.mobile,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        question1: { q1: q1, a1: a1 },
        question2: { q2: q2, a2: a2 },
        status: true,
        token: "",
        role: req.body.role,
        googleId: ""
    })
    register.save(function (err, result) {
        if (err) {
            console.log("User Registration Failed");
            res.send({ status: false, message: consts.fail_message, err });
        } else {
            var profile = new Profile({
                name: req.body.name,
                mobile: req.body.mobile,
                email: req.body.email,
                userId: result._id,
                gender: req.body.gender,
                dob: new Date("2013-10-01T00:00:00.000Z"),
                courses: [],
                quiz: [],
                image: ""
            });
            profile.save(function (err) {
                if (err) {
                    console.log(err)
                }
            });
            register.userProfile = profile;
            // register.userProfile.push(profile);
            register.save();
            console.log("User Registered Successfully");
            res.send({ status: true, message: consts.success_message });
        }
    });
}

exports.loginUser = function (req, res) {
    // Register.findOne({"username": req.body.username, "password": req.body.password}, function(err, result) {
    //     if (err){ 
    //         res.send({status: false, message: consts.fail, err});
    //         return console.error(err);
    //     } else {
    //         console.log(result)
    //         if(result == null){
    //             res.send({status: false, message: consts.fail});
    //         } else {
    //             result = result.toObject();
    //             delete result.password;
    //             res.send({status: true, message: consts.success, result});
    //         }
    //     }
    // });
    Register.findOne({ "username": { $regex: new RegExp(req.body.username, "i") }, "password": req.body.password, "status": true }).populate('userProfile').exec(function (err, result) {
        if (err) {
            console.log(err)
        } else {
            if (result == null) {
                res.send({ status: false, message: consts.fail });
            } else {
                result = result.toObject();
                delete result.password;
                res.send({ status: true, message: consts.success, result });
            }
        }
    });
}

//Change Password
exports.changePassword = function (req, res) {
    Register.count({ email: req.body.email, password: req.body.oldPass }, function (err, result) {
        if (err) {
            res.send({ status: false, message: "error occurred", err });
        }
        else {
            if (result == 1) {
                Register.update({ email: req.body.email }, { $set: { password: req.body.newPass } }, function (err, result) {
                    res.send({ status: true, message: "password has been modified" });
                });
            } else {
                res.send({ status: false, message: "password dosent match" });
            }
        }
    });
}

//Get a particular user register information
exports.getUserProfile = function (req, res) {
    Register.findById(req.params.id).populate('userProfile').exec(function (err, result) {
        if (err) {
            console.log(err)
        } else {
            if (result == null) {
                res.send({ status: false, message: consts.fail });
            } else {
                result = result.toObject();
                delete result.password;
                console.log(result);
                res.send({ status: true, message: consts.success, result });
            }
        }
    });
}
exports.getSecurityQues = function (req, res) {
    Register.findOne({ email: req.body.email }, function (err, result) {
        if (err) {
            console.error(err);
        }
        else {
            var userView = {
                _id: true,
                "question1.q1": true,
                "question2.q2": true
            }
            Register.findOne({ email: req.body.email }, userView, function (err, result) {
                if (err) {
                    console.log(err);
                }
                else {
                    res.send({ status: true, message: consts.success, result });
                }
            });
            //Register.findOneAndUpdate({email:email},{$set:{password:req.body.password}});
        }
    });
}

//Forget Password
exports.forgetPassword = function (req, res) {
    var token = randtoken.generate(16);
    Register.findOneAndUpdate({ email: req.body.email, "question1.a1": req.body.answer1, "question2.a2": req.body.answer2 },
        { $set: { password: token } }, function (err, result) {
            if (err) {
                console.log("password doesn't change", err);
                res.send({ status: false, message: consts.fail });
            }
            else {
                var transporter = nodemailer.createTransport({
                    host: 'smtp.gmail.com',
                    port: 587,
                    auth: {
                        user: 'huntjohns1988@gmail.com',
                        pass: 'Huntjohns@1988' //You have to turn of secure access from gmail "Review blocked sign-in attempt"
                    },
                    tls: { rejectUnauthorized: false },
                    debug: true
                });
                var mailOptions = {
                    from: "no-reply@gmail.com",
                    to: req.body.email,
                    subject: "ATMECS ELearning Password Reset",
                    html: 'Hello <strong>' + result.name +
                    '</strong>,<br><br> Your password has been modified.<br>Your new Password is <strong>' + token +
                    '</strong>.<br>For more information contact us on: info@atmecs.com <br><br>Thank you & Regards,<br>Atmecs E-Learning Team'
                };

                // send mail with defined transport object
                transporter.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        return console.log(error);
                    } else {
                        console.log('Message %s sent: %s' + info.res);
                        res.send({ status: true, message: consts.success, result });
                    };
                });
            }
        });
}

//Update User/Admin Role (For Super Admin)
exports.updateRole = function (req, res) {
    if (req.params.role == 0) {
        role = "USER";
    } else if (req.params.role == 1) {
        role = "ADMIN";
    } else {
        res.send({ status: false, message: "Wrong input role" });
    }
    Register.findOneAndUpdate({ _id: req.params.userId }, { $set: { role: role } }, function (err, result) {
        if (err) {
            res.send({ status: false, message: consts.fail });
        }
        else {
            res.send({ status: true, message: "Role Updated" });
        }
    });
}

//Update User/Admin Status (For Super Admin)
exports.updateStatus = function (req, res) {
    let status;
    if (req.params.status == 0) {
        status = false;
    } else if (req.params.status == 1) {
        status = true;
    } else {
        res.send({ status: false, message: "Wrong input status" });
    }
    Register.findOneAndUpdate({ _id: req.params.userId }, { $set: { status: status } }, function (err, result) {
        if (err) {
            res.send({ status: false, message: consts.fail });
        }
        else {
            res.send({ status: true, message: "Status Updated" });
        }
    });
}

//Get All User (For Super Admin)
exports.getAllUser = function (req, res) {
    var registerView = {
        _id: true,
        name: true,
        email: true,
        username: true,
        status: true,
        role: true,
        userProfile: true
    }

    var profileView = {
        gender: true
    }

    Register.find({}, registerView).populate('userProfile', profileView).exec(function (err, result) {
        if (err) {
            console.log(err)
        } else {
            res.send({ status: true, message: consts.success, result });
        }
    });
}

exports.verifyEmail = function (req, res) {
    Register.count({ email: req.params.email }, function (err, result) {
        if (err)
            res.send({ status: false, message: consts.fail, err });
        else {
            res.send({ status: true, message: consts.success, result });
        }
    });
}

exports.verifyUsername = function(req, res){
    Register.count({ username: req.params.username }, function(err, result){
        if (err)
        res.send({ status: false, message: consts.fail, err });
        else{
            res.send({ status: true, message: consts.success, result });
        }
    });
}